package jp.co.aainc.watav.common.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.model.Event;

/**
 * Base Events Adapter
 * Created by TuanDT on 12/1/2014.
 */
public abstract class BaseAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {
    private static final String HTTP_PREFIX = "http://";
    private static final String HTTPS_PREFIX = "https://";

    // Type of rows
    protected static final int TYPE_FOOTER = 0;
    protected static final int TYPE_ITEM = 1;

    private List<Event> mEvents;
    private Context mContext;

    private boolean mCanLoadMore;

    // Provide a suitable constructor (depends on the kind of data-set)
    public BaseAdapter(Context context, List<Event> events) {
        mEvents = events;
        mContext = context;
        mCanLoadMore = true;
    }

    public Event getItem(int position) {
        if (mEvents == null || position == mEvents.size())
            return null;

        return mEvents.get(position);
    }

    /**
     * Get Context
     *
     * @return Context
     */
    protected Context getContext() {
        return mContext;
    }

    /**
     * Get Events count
     * @return size of list
     */
    public int getEventsCount() {
        return mEvents.size();
    }

    // Return the size of your data set (invoked by the layout manager)
    @Override
    public int getItemCount() {
        // If no data
        if (mEvents == null || mEvents.size() == 0)
            return 0;

        // If has more items
        if (mCanLoadMore) {
            return mEvents.size() + 1;
        } else {
            return mEvents.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mEvents.size())
            return TYPE_FOOTER;

        return TYPE_ITEM;
    }

    public void setCanLoadMore(boolean canLoadMore) {
        mCanLoadMore = canLoadMore;
    }

    /**
     * Get Footer view: Progress bar
     *
     * @param inflater Layout inflater
     * @param parent Parent view
     * @return Footer view holder
     */
    protected VHFooter getFooterHolder(LayoutInflater inflater, ViewGroup parent) {
        return new VHFooter(inflater.inflate(R.layout.layout_list_footer, parent, false));
    }

    protected class VHFooter extends RecyclerView.ViewHolder {
        public VHFooter(View itemView) {
            super(itemView);
        }
    }
}
