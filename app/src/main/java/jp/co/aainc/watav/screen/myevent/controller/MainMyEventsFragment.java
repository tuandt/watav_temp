package jp.co.aainc.watav.screen.myevent.controller;

import android.app.Activity;
import android.support.v4.app.Fragment;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.screen.main.controller.MainActivity;
import jp.co.aainc.watav.screen.main.controller.MainFragment;
import jp.co.aainc.watav.screen.myevent.history.controller.MyHistoryEventsFragment;

/**
 * Main Fragment controller for my events screen
 * Created by TuanDT on 12/11/2014.
 */
public class MainMyEventsFragment extends MainFragment implements OnHistoryEventsListener {
    private MainActivity mActivity;

    @Override
    protected Fragment onCreateFirstFragment() {
        return new MyEventsFragment();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden && (getChildFragmentManager().findFragmentById(ID_ROOT_FRAME) instanceof MyHistoryEventsFragment)) {
            mActivity.getActionBarHelper().setColor(getResources().getColor(R.color.my_page_history_bg));
            mActivity.getActionBarHelper().setTitle(R.string.title_my_history_events);
        }
    }

    @Override
    public void onHistoryOpen() {
        mActivity.getActionBarHelper().setColor(getResources().getColor(R.color.my_page_history_bg));
        mActivity.getActionBarHelper().setTitle(R.string.title_my_history_events);
        DialogUtil.showProgressDialog(mActivity);
        addFragment(MyHistoryEventsFragment.newInstance(true), true);
    }

    @Override
    public void onHistoryClose() {
        mActivity.getActionBarHelper().setBackground(R.drawable.bg_actionbar);
        mActivity.getActionBarHelper().setTitle(R.string.title_my_events);
    }
}
