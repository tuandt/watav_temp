package jp.co.aainc.watav.request.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by TuanDT on 11/14/2014.
 */
public class ErrorResponse {
    @SerializedName("errorCode")
    private int mErrorCode;

    @SerializedName("errorMessage")
    private String mErrorMessage;

    public int getErrorCode() {
        return mErrorCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
}
