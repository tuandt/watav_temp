package jp.co.aainc.watav.screen.login.controller;

import android.app.Activity;

import com.android.volley.Request;

import java.util.Map;

import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.request.manager.BaseRequest;
import jp.co.aainc.watav.request.manager.ForegroundRequest;
import jp.co.aainc.watav.screen.login.model.LoginData;

/**
 * Request login with Facebook info & device info
 * Created by TuanDT on 11/14/2014.
 */
public class LoginRequest extends ForegroundRequest<LoginData> {
    private static final String TAG = LoginRequest.class.getSimpleName();

    public LoginRequest(Activity activity, Class<LoginData> clazz, Map<String, String> params, BaseRequest.BaseListener<LoginData> listener) {
        super(activity, clazz, params, listener);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.LOGIN;
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected int getMethod() {
        return Request.Method.POST;
    }
}
