package jp.co.aainc.watav.screen.myevent.history.controller;

import android.app.Activity;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DateUtil;

/**
 * Favorite events fragment
 * Created by TuanDT on 12/3/2014.
 */
public class MyHistoryFavoritedEventsFragment extends ListEventsFragment {

    @Override
    protected String getUrl() {
        return Requests.Methods.LIST_EVENT;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_list_item_common;
    }

    @Override
    protected boolean loadBlurCover() {
        return true;
    }

    @Override
    protected Event.TimeFormat getTimeFormat() {
        return Event.TimeFormat.TIME_FORMAT_HTML;
    }

    @Override
    protected Map<String, String> getParams(Activity activity) {
        Bundle args = getArguments();

        if (args == null) {
            return new HashMap<>();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(activity));
        params.put(Requests.Params.EVENT_FAVORITE, "1");
        params.put(Requests.Params.SORT, Requests.Values.SORT_END_DATE);
        params.put(Requests.Params.ORDER, Requests.Values.ORDER_DESC);
        params.put(Requests.Params.END_DATE_TO, DateUtil.parseJapanCurrentTime());
        return params;
    }
}
