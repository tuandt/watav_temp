package jp.co.aainc.watav.screen.recommend.controller;

/**
 * Show all Button clicked listener
 * Created by TuanDT on 11/27/2014.
 */
public interface OnShowAllClicked {
    public void onShowAllClicked(String title);
}
