package jp.co.aainc.watav.screen.search.controller;

import com.android.volley.Request;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.model.Category;
import jp.co.aainc.watav.request.manager.BaseRequest;

/**
 * Request get Categories data
 * Created by TuanDT on 12/03/2014.
 */
public class CategoriesRequest extends BaseRequest<List<Category>> {
    private static final String TAG = CategoriesRequest.class.getSimpleName();

    public CategoriesRequest(Type type, Map<String, String> params, BaseListener<List<Category>> listener) {
        super(type, params, listener);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.CATEGORIES;
    }

    @Override
    protected int getMethod() {
        return Request.Method.GET;
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}

