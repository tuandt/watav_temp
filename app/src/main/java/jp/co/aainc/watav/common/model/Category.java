package jp.co.aainc.watav.common.model;

import com.google.gson.annotations.SerializedName;

/**
 * Event Category
 * Created by TuanDT on 11/18/2014.
 */
public class Category {
    @SerializedName("id")
    private int mId;

    @SerializedName("name")
    private String mName;

    @SerializedName("level")
    private int mLevel;

    public int getId() {
        return mId;
    }

    public int getLevel() {
        return mLevel;
    }

    public String getName() {
        return mName;
    }
}
