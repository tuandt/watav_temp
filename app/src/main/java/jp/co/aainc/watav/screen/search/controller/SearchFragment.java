package jp.co.aainc.watav.screen.search.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.TabHost;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.controller.ListPagersAdapter;
import jp.co.aainc.watav.common.controller.PagersFragment;
import jp.co.aainc.watav.common.model.Category;
import jp.co.aainc.watav.common.util.NetworkUtil;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Search fragment
 * Created by TuanDT on 12/03/2014.
 */
public class SearchFragment extends PagersFragment {
    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tabs_scrollable;
    }

    @Override
    protected ListPagersAdapter createAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager viewPager) {
        // Use default PagerAdapter
        return null;
    }

    @Override
    protected void initializeData(Activity activity) {
        getCategories(activity);
    }

    private void getCategories(Activity activity) {
        if (NetworkUtil.checkNetwork(activity)) {
            Type collectionType = new TypeToken<Collection<Category>>() {
            }.getType();
            new CategoriesRequest(collectionType, null, mCategoriesListener).send();
        }
    }

    private RequestListener mCategoriesListener = new RequestListener<List<Category>>() {
        @Override
        public void onSuccess(List<Category> data) {
            initPager(data);
        }
    };

    private void initPager(List<Category> categories) {
        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);
            Bundle args = new Bundle();
            args.putInt(Requests.Params.CATEGORY_ID, category.getId());
            args.putBoolean(ListEventsFragment.KEY_INITIAL_ON_CREATE, false);
            addTab(category.getName(), CategoryFragment.class, args, "category " + category.getId());
        }
        notifyDataSetChanged(true);
    }
}
