package jp.co.aainc.watav.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.TextView;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.util.Typefaces;

/**
 * Custom font for English text
 * Created by TuanDT on 11/26/2014.
 */
public class CustomTextView extends TextView{
    public CustomTextView(Context context) {
        super(context);
        setTypeFace(null);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeFace(attrs);
    }

    private void setTypeFace(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextView);
        String fontPath = a.getString(R.styleable.TextView_typeFaceFont);
        if (fontPath != null) {
            this.setTypeface(Typefaces.get(getContext(), fontPath));
        }
    }
}
