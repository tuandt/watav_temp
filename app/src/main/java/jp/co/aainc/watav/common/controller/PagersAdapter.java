package jp.co.aainc.watav.common.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;
import android.widget.TabWidget;

import java.util.ArrayList;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Common Tabs adapter
 * Created by Admin on 12/3/2014.
 */
public class PagersAdapter extends FragmentPagerAdapter
implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {
    private static final String TAG = PagersAdapter.class.getName();

    protected MainActivity mActivity;
    protected TabHost mTabHost;
    protected ViewPager mViewPager;
    protected ArrayList<TabInfo> mTabs = new ArrayList<>();
    private HorizontalScrollView mHorizontalScroll;
    protected FragmentManager mFragmentManager;

    protected class TabInfo {
        private final String mTag;
        private final Class<?> mClazz;
        private final Bundle mArgs;

        public TabInfo(String tag, Class<?> clazz, Bundle args) {
            mTag = tag;
            mClazz = clazz;
            mArgs = args;
        }
    }

    public class TabFactory implements TabHost.TabContentFactory {
        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }

    public PagersAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager pager) {
        super(manager);
        mFragmentManager = manager;
        mActivity = activity;
        mTabHost = tabHost;
        mViewPager = pager;
        mTabHost.setOnTabChangedListener(this);
        mViewPager.setAdapter(this);
        mViewPager.setOnPageChangeListener(this);
        mHorizontalScroll = (HorizontalScrollView) mTabHost.findViewById(R.id.tab_widget_scroll);
    }

    public void addTab(String title,Class<?> clazz, Bundle args, String tag) {
        TabHost.TabSpec tabSpec = mTabHost.newTabSpec(tag).setIndicator(title);
        tabSpec.setContent(new TabFactory(mActivity));

        TabInfo info = new TabInfo(tag, clazz, args);
        mTabs.add(info);
        if (mTabHost.getChildCount() > 0)
            notifyDataSetChanged();
        mTabHost.addTab(tabSpec);

        // Keep number of page states
        mViewPager.setOffscreenPageLimit(mTabs.size());
    }

    @Override
    public int getCount() {
        return mTabs.size();
    }

    @Override
    public Fragment getItem(int position) {
        TabInfo info = mTabs.get(position);
        Fragment f = Fragment.instantiate(mActivity, info.mClazz.getName(), info.mArgs);
        return f;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (mHorizontalScroll == null) {
            return;
        }
        View tabView = mTabHost.getTabWidget().getChildAt(position);
        if (tabView != null) {
            final int width = mHorizontalScroll.getWidth();
            final int scrollPos = tabView.getLeft() - (width - tabView.getWidth()) / 2;
            mHorizontalScroll.scrollTo(scrollPos, 0);
        } else {
            mHorizontalScroll.scrollBy(positionOffsetPixels, 0);
        }
    }

    @Override
    public void onTabChanged(String tabId) {
        int position = mTabHost.getCurrentTab();
        notifyDataSetChanged();
        mViewPager.setCurrentItem(position, false);
    }

    @Override
    public void onPageSelected(int position) {
        // Unfortunately when TabHost changes the current tab, it kindly
        // also takes care of putting focus on it when not in touch mode.
        // The jerk.
        // This hack tries to prevent this from pulling focus out of our
        // ViewPager.
        TabWidget widget = mTabHost.getTabWidget();
        int oldFocusability = widget.getDescendantFocusability();
        widget.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        mTabHost.setCurrentTab(position);
        widget.setDescendantFocusability(oldFocusability);
        if (position == 0) {
            mActivity.onAtStartPosition();
        } else {
            mActivity.onAtMiddlePosition();
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }
}
