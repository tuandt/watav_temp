package jp.co.aainc.watav.screen.main.controller;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import jp.co.aainc.watav.common.controller.BaseActivity;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.recommend.controller.OnHorizontalScrollListener;
import jp.co.aainc.watav.screen.recommend.controller.RecommendFragment;


public class MainActivity extends BaseActivity implements
        OnHorizontalScrollListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switchMainView(ID_DEFAULT_FRAGMENT);
    }

    @Override
    public void onAtStartPosition() {
        Logger.i("onAtStartPosition");
        mSlidingLayout.setCanSlide(true);
        setPagerScroll(false);
    }

    @Override
    public void onAtEndPosition() {
        Logger.w("onAtEndPosition");
        mSlidingLayout.setCanSlide(false);
        setPagerScroll(true);
    }

    @Override
    public void onAtMiddlePosition() {
        Logger.d("onAtMiddlePosition");
        mSlidingLayout.setCanSlide(false);
        setPagerScroll(false);
    }

    @Override
    public void onScrolled() {
        mSlidingLayout.setCanSlide(true);
        setPagerScroll(true);
    }

    private void setPagerScroll(boolean canScroll) {
        FragmentManager manager = getSupportFragmentManager();
        Fragment fragment = manager.findFragmentById(ID_MAIN_FRAGMENT_LAYOUT);
        if (fragment instanceof RecommendFragment) {
            ((RecommendFragment) fragment).setPagerCanScroll(canScroll);
        }
    }

    public ActionBarHelper getActionBarHelper() {
        return mActionBar;
    }
}
