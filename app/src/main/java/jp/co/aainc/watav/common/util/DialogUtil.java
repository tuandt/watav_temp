package jp.co.aainc.watav.common.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.provider.Settings;
import android.view.Gravity;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.log.Logger;


/**
 * @author nhatnd
 */
@SuppressLint("SimpleDateFormat")
public class DialogUtil {
    public static final int ID_BUTTON_OK = 12;
    public static final int ID_BUTTON_CANCEL = 13;

    private static AlertDialog sAlert;
    private static ProgressDialog sProgress;

    /**
     * Show dialog progress.
     *
     * @param activity the context is running.
     */
    public static void showProgressDialog(final Activity activity) {
        if (activity == null || activity.isFinishing()) {
            Logger.e("showProgressDialog with null Activity");
            return;
        }

        // Dismiss alert dialog if on showing
        if (sAlert != null && sAlert.isShowing()) {
            sAlert.dismiss();
        }

        // Dismiss progress dialog if on showing
        if (sProgress != null && sProgress.isShowing()) {
            sProgress.dismiss();
        }

        // Create new one & show
        sProgress = new ProgressDialog(activity);
        sProgress.setMessage(activity.getString(R.string.msg_loading));
        sProgress.setCancelable(false);
        sProgress.show();
    }

    /**
     * Dismiss progress.
     *
     * @param activity the context is running.
     */
    public static void dismissProgressDialog(final Activity activity) {
//        try {
//            if (activity != null && !activity.isFinishing()) {
//                if (sProgress != null && sProgress.isShowing()) {
//                    sProgress.dismiss();
//                }
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        if (sProgress != null && sProgress.isShowing()) {
            sProgress.dismiss();
        }
    }

    /**
     * Show dialog title and message.
     *
     * @param context the context is running.
     * @param title   of dialog.
     * @param message of dialog.
     */
    public static void showDialogMessage(final Context context,
                                         final String title, final String message) {

        final Builder builder = new Builder(context);
        if (title != null)
            builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.button_ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        sAlert = builder.create();
        sAlert.show();
        TextView messageView = (TextView) sAlert
                .findViewById(android.R.id.message);
        if (messageView != null) {
            messageView.setGravity(Gravity.CENTER);
        }
    }

    /**
     * @param context  the context is running.
     * @param title    of dialog.
     * @param message  of dialog.
     * @param listener callback when clicked button ok.
     * @param isCancel true show button cancel.
     */
    public static void showDialogMessage(final Context context,
                                         final String title, final String message,
                                         final OnClickListener listener, final boolean isCancel) {
        final Builder builder = new Builder(context);
        if (title != null)
            builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.button_ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener != null) {
                    listener.onClick(dialog, ID_BUTTON_OK);
                }
                dialog.dismiss();
            }
        });
        if (isCancel) {
            builder.setPositiveButton(R.string.button_cancel,
                    new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (listener != null) {
                                listener.onClick(dialog,
                                        ID_BUTTON_CANCEL);
                            }
                            dialog.dismiss();
                        }
                    });
        }
        sAlert = builder.create();
        sAlert.show();
        TextView messageView = (TextView) sAlert
                .findViewById(android.R.id.message);
        if (messageView != null) {
            messageView.setGravity(Gravity.CENTER);
        }
    }

    /**
     * Show dialog date Picker.
     *
     * @param context    {@link android.content.Context} to show Dialog
     * @param callBack   Date set listener
     * @param formatDate sample yyyy-MM-dd.
     * @param data       2014-10-30
     * @return {@link android.app.DatePickerDialog}
     */
    public static DatePickerDialog showDialogDatePicker(final Context context,
                                                        final OnDateSetListener callBack, final String formatDate,
                                                        final String data) {
        int year = 0;
        int monthOfYear = 0;
        int dayOfMonth = 0;
        if (formatDate != null && data != null) {
            SimpleDateFormat df = new SimpleDateFormat(formatDate);
            try {
                Date date = df.parse(data);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                year = calendar.get(Calendar.YEAR);
                monthOfYear = calendar.get(Calendar.MONTH);
                dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        DatePickerDialog dialog = new DatePickerDialog(context, callBack, year,
                monthOfYear, dayOfMonth);
        dialog.show();
        return dialog;
    }

    /**
     * Show dialog when request error
     *
     * @param activity Activity show dialog
     */
    public static void showRequestErrorDialog(final Activity activity) {
        DialogUtil.showDialogMessage(activity, activity.getString(R.string.msg_error),
                activity.getString(R.string.msg_request_fail),
                null, false);
    }

    /**
     * Show dialog when network error
     *
     * @param activity Activity show dialog
     */
    public static void showNetworkErrorDialog(final Activity activity) {
        // Open Wireless setting
        OnClickListener listener = new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (activity != null && !activity.isFinishing()) {
                    activity.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
                    activity.finish();
                }
            }
        };

        // Show dialog
        DialogUtil.showDialogMessage(activity, activity.getString(R.string.title_network_lost),
                activity.getString(R.string.msg_network_lost), listener,
                false);
    }
}
