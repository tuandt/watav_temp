package jp.co.aainc.watav.common.controller;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.TabHost;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Common List Tabs adapter
 * Created by Admin on 12/3/2014.
 */
public class ListPagersAdapter extends PagersAdapter {
    public ListPagersAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager pager) {
        super(activity, manager, tabHost, pager);
    }

    @Override
    public void onPageSelected(int position) {
        super.onPageSelected(position);
        checkTabInitialized(position);
    }

    private void checkTabInitialized(int position) {
        Logger.e("checkTabInitialized  " + position);
        Logger.e(position + "-" + mViewPager.getCurrentItem());

        // Initial data
        String tag = "android:switcher:" + R.id.pager + ":" + position;
        Fragment fragment = mFragmentManager.findFragmentByTag(tag);
        Logger.d("Get fragment from ViewPager " + fragment);
        if (fragment instanceof ListEventsFragment) {
            ListEventsFragment f = (ListEventsFragment) fragment;
            f.initData(mActivity);
        }
    }
}
