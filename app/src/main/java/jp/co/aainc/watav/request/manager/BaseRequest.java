package jp.co.aainc.watav.request.manager;

import android.net.Uri;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.request.model.BaseResponse;

/**
 * Base server request
 * Created by Admin on 11/20/2014.
 */
public abstract class BaseRequest<DATA> implements Response.Listener<BaseResponse>, Response.ErrorListener {
    public static final String TAG = BaseRequest.class.getSimpleName();

    // Common error code
    public static final int ERROR_CODE_COMMON = 9999;

    // Parameters
    private Map<String, String> mParams = new HashMap<>();

    // Listener
    private BaseListener<DATA> mRequestListener;

    // Data class
    private Class<DATA> mClass;
    private Type mType;

    public BaseRequest(Class<DATA> clazz, Map<String, String> params, BaseListener<DATA> listener) {
        mParams = params;
        mClass = clazz;
        mRequestListener = listener;
    }

    public BaseRequest(Type type, Map<String, String> params, BaseListener<DATA> listener) {
        mParams = params;
        mType = type;
        mRequestListener = listener;
    }

    /**
     * Send request
     */
    public void send() {
        final GsonRequest<Response> request = new GsonRequest(getMethod(), buildUrl(), BaseResponse.class,
                getHeaders(), mParams, this, this);
        String tag = getTag();
        request.setTag(tag == null || tag.length() <= 0 ? TAG : tag);
        RequestQueueManager.getRequestQueue().add(request);
    }

    @Override
    public void onResponse(BaseResponse response) {
        if (mRequestListener == null) {
            return;
        }

        if (response.isSuccess()) {
            DATA data = null;
            if (mClass != null) {
                data = new Gson().fromJson(response.getData(), mClass);
            }  else if (mType != null) {
                data = new Gson().fromJson(response.getData(), mType);
            }
            mRequestListener.onRetrievedData(data);
        } else {
            if (response.getError() == null) {
                mRequestListener.onError(ERROR_CODE_COMMON, null);
            } else {
                mRequestListener.onError(response.getError().getErrorCode(), response.getError().getErrorMessage());
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (mRequestListener != null) {
            mRequestListener.onError(ERROR_CODE_COMMON, error.getMessage());
        }
    }

    /**
     * Build Url request for POST or GET method
     *
     * @return
     */
    private String buildUrl() {
        String url;
        final String PARAMS_FORMAT = "%1$s=%2$s&";

        if (getMethod() == Request.Method.GET) {
            url = getUrl() + "?";

            // Parameters
            String params = "";
            if (mParams != null && mParams.size() > 0) {
                GsonRequest.show("======= set Parameter =======", mParams);
                for (String key : mParams.keySet()) {
                    params += (String.format(PARAMS_FORMAT, key, Uri.encode(mParams.get(key))));
                }

                // Add params to url
                url += params;
            }
        } else {
            url = getUrl();
        }

        return url;
    }

    /**
     * Request url
     *
     * @return URL
     */
    protected abstract String getUrl();

    /**
     * Request method POST or GET
     *
     * @return Method
     */
    protected abstract int getMethod();

    /**
     * Request TAG
     *
     * @return tag of request
     */
    protected abstract String getTag();

    /**
     * Set parameters for request
     *
     * @param params parameters
     */
    public void setParams(Map<String, String> params) {
        mParams = params;
    }

    /**
     * Request Headers
     *
     * @return Headers
     */
    protected Map<String, String> getHeaders() {
        return new HashMap<String, String>();
    }

    /**
     * Base Request listener
     *
     * @param <T>
     */
    public interface BaseListener<T> {
        public void onError(int errorCode, String errorMsg);

        public void onRetrievedData(T data);
    }
}
