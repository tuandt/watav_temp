package jp.co.aainc.watav.common.controller;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.view.CustomSlidingPanelLayout;
import jp.co.aainc.watav.screen.main.controller.ActionBarHelper;
import jp.co.aainc.watav.screen.myevent.controller.MainMyEventsFragment;
import jp.co.aainc.watav.screen.recommend.controller.MainRecommendFragment;
import jp.co.aainc.watav.screen.search.controller.MainSearchFragment;
import jp.co.aainc.watav.screen.setting.controller.SettingFragment;

/**
 * Base Activity with base methods & Variables
 * Created by TuanDT on 11/12/2014.
 */
public class BaseActivity extends FragmentActivity {
    protected static final int ID_MAIN_FRAGMENT_LAYOUT = R.id.main_fragment_layout;

    protected static final int ID_RECOMMEND_FRAGMENT = 0;
    protected static final int ID_SEARCH_FRAGMENT = 1;
    protected static final int ID_MY_EVENTS_FRAGMENT = 2;
    protected static final int ID_SETTING_FRAGMENT = 3;
    protected static final int ID_DEFAULT_FRAGMENT = ID_RECOMMEND_FRAGMENT;

    // Sliding menu layout
    protected CustomSlidingPanelLayout mSlidingLayout;

    // Action bar
    protected ActionBarHelper mActionBar;

    // Left sliding menu list
    private String[] mMenuList;

    // Current screen
    private int mCurrentFragmentIndex = 0;

    // Main fragments
    private Map<Integer, Fragment> mMainFragments = new HashMap<>();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        init();
        initMainFragment();
    }

    /**
     * Initialize common settings
     */
    private void init() {
        // Sliding panel
        mSlidingLayout = (CustomSlidingPanelLayout) findViewById(R.id.sliding_pane_layout);
        mSlidingLayout.setPanelSlideListener(new SliderListener());
        mSlidingLayout.getViewTreeObserver().addOnGlobalLayoutListener(new FirstLayoutListener());

        // Action bar
        mActionBar = new ActionBarHelper(this);
        mActionBar.init();

        // Left panel
        ListView listView = (ListView) findViewById(R.id.left_panel_list_view);
        mMenuList = getResources().getStringArray(R.array.menu);
        listView.setAdapter(new ArrayAdapter<>(this, R.layout.menu_item,
                mMenuList));
        listView.setOnItemClickListener(new ListItemClickListener());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Don't call super to fix findFragmentByTag bug
    }

    public SlidingPaneLayout getPaneLayout() {
        return mSlidingLayout;
    }

    /**
     * This panel slide listener updates the action bar accordingly for each panel state.
     */
    private class SliderListener extends SlidingPaneLayout.SimplePanelSlideListener {
        @Override
        public void onPanelOpened(View panel) {
            mActionBar.onPanelOpened();
        }

        @Override
        public void onPanelClosed(View panel) {
            mActionBar.onPanelClosed();
        }
    }

    /**
     * This global layout listener is used to fire an event after first layout occurs
     * and then it is removed. This gives us a chance to configure parts of the UI
     * that adapt based on available space after they have had the opportunity to measure
     * and layout.
     */
    private class FirstLayoutListener implements ViewTreeObserver.OnGlobalLayoutListener {
        @Override
        public void onGlobalLayout() {
            mActionBar.onFirstLayout();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mSlidingLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        }
    }

    /**
     * Left menu item click listener
     */
    private class ListItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (mCurrentFragmentIndex != position) {
                switchMainView(position);
                mCurrentFragmentIndex = position;
            }

            mSlidingLayout.closePane();
        }
    }

    protected void switchMainView(int id) {
        // Hide all and show fragment at position
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();
        for (Fragment f : mMainFragments.values()) {
            transaction.hide(f);
        }

        transaction.show(getMainFragment(id));
        transaction.commit();

        // Check visible screen to update action bar
        checkToUpdateActionBar(id);
    }

    private void checkToUpdateActionBar(int id) {
        if (id < 0) {
            return;
        }
        mActionBar.setBackground(R.drawable.bg_actionbar);
        mActionBar.setTitle(mMenuList[id]);
    }


    private Fragment getMainFragment(int id) {
        return mMainFragments.get(id);
    }

    private void initMainFragment() {
        mMainFragments.put(ID_RECOMMEND_FRAGMENT, new MainRecommendFragment());
        mMainFragments.put(ID_SEARCH_FRAGMENT, new MainSearchFragment());
        mMainFragments.put(ID_MY_EVENTS_FRAGMENT, new MainMyEventsFragment());
        mMainFragments.put(ID_SETTING_FRAGMENT, new SettingFragment());

        // Add all to FragmentTransaction
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        for (Fragment f : mMainFragments.values()) {
            transaction.add(ID_MAIN_FRAGMENT_LAYOUT, f);
            transaction.hide(f);
        }

        transaction.commit();
    }

    /**
     * Process child fragments back stacks
     *
     * @param fm Child fragment managers
     * @return True if has fragment handle back press
     */
    private boolean onBackPressed(FragmentManager fm) {
        if (fm != null) {
            if (fm.getBackStackEntryCount() > 0) {
                fm.popBackStack();
                return true;
            }

            List<Fragment> fragList = fm.getFragments();
            if (fragList != null && fragList.size() > 0) {
                for (Fragment frag : fragList) {
                    if (frag == null) {
                        continue;
                    }
                    if (frag.isVisible()) {
                        if (onBackPressed(frag.getChildFragmentManager())) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (onBackPressed(fm)) {
            return;
        }
        super.onBackPressed();
    }
}
