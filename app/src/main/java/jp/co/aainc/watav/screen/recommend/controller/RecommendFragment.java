package jp.co.aainc.watav.screen.recommend.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.ListPagersAdapter;
import jp.co.aainc.watav.common.controller.PagersFragment;
import jp.co.aainc.watav.screen.main.controller.MainActivity;
import jp.co.aainc.watav.screen.ranking.controller.RankingFragment;

/**
 * Top fragment with 2 tabs: suggest & ranking
 * Created by TuanDT on 12/1/2014.
 */
public class RecommendFragment extends PagersFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mPagersAdapter.addTab(getString(R.string.title_recommend), SuggestFragment.class, null, "suggest");
//        mPagersAdapter.addTab(getString(R.string.title_ranking), DummyFragment.class, null, "ranking");
        mPagersAdapter.addTab(getString(R.string.title_ranking), RankingFragment.class, null, "ranking");

        mPagersAdapter.notifyDataSetChanged();

        return view;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tabs_fixed;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("tab", mTabHost.getCurrentTabTag());
    }

    @Override
    protected ListPagersAdapter createAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager viewPager) {
        return null;
    }

    @Override
    protected void initializeData(Activity activity) {

    }

    public void setPagerCanScroll(boolean canScroll) {
        int idx = mViewPager.getCurrentItem();
        mViewPager.setCurrentItem(idx + 1, true);
        mViewPager.setCanScroll(canScroll);
    }
}
