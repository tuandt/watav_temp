package jp.co.aainc.watav.screen.search.controller;

import android.app.Activity;
import android.os.Bundle;

import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DateUtil;

/**
 * Events list in category fragment
 * Created by TuanDT on 12/3/2014.
 */
public class CategoryFragment extends ListEventsFragment {
    @Override
    protected String getUrl() {
        return Requests.Methods.LIST_EVENT;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_list_item_common;
    }

    @Override
    protected boolean loadBlurCover() {
        return true;
    }

    @Override
    protected Event.TimeFormat getTimeFormat() {
        return Event.TimeFormat.TIME_FORMAT_HTML;
    }

    @Override
    protected Map<String, String> getParams(Activity activity) {
        Bundle args = getArguments();

        if (args == null) {
            return new HashMap<>();
        }

        Map<String, String> params = new HashMap<String, String>();
        params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(activity));
        params.put(Requests.Params.SORT, Requests.Values.SORT_FROM_DATE);
        params.put(Requests.Params.ORDER, Requests.Values.ORDER_ASC);
        int catId = args.getInt(Requests.Params.CATEGORY_ID);
        params.put(Requests.Params.CATEGORY_ID, String.valueOf(catId));
        params.put(Requests.Params.EVENT_HIDDEN, "0");
        params.put(Requests.Params.START_DATE_FR, DateUtil.parseCurrentTimeWithoutHour());
        return params;
    }
}
