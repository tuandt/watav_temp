package jp.co.aainc.watav.common.model;

import android.text.Spanned;
import android.text.format.DateUtils;
import android.widget.TextView;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import jp.co.aainc.watav.common.util.DateUtil;

/**
 * Event model
 * Created by TuanDT on 11/18/2014.
 */
public class Event {
    private static final long NEW_TIME_EVENT = 2 * DateUtils.DAY_IN_MILLIS; // 2 days

    public enum TimeFormat {
        TIME_FORMAT_HTML,
        TIME_FORMAT_START_1_LINE,
        TIME_FORMAT_START_2_LINE
    }

    @SerializedName("event_id")
    private int mId;

    @SerializedName("event_favorite")
    private String mEventFavorite;

    @SerializedName("event_title")
    private String mTitle;

    @SerializedName("event_detail")
    private String mDetail;

    @SerializedName("event_image_url")
    private String mImageUrl;

    @SerializedName("event_favorite_total")
    private int mFavoriteTotal;

    @SerializedName("event_from_date")
    private String mFromDate;

    @SerializedName("event_end_date")
    private String mEndDate;

    @SerializedName("date_created")
    private String mDateCreated;

    @SerializedName("event_category")
    private List<Category> mCategories;

    public int getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getDetail() {
        return mDetail;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public int getFavoriteTotal() {
        return mFavoriteTotal;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public List<Category> getCategories() {
        return mCategories;
    }

    /**
     * Get time event with formats in {@link jp.co.aainc.watav.common.model.Event.TimeFormat}
     * @param timeFormat Time format
     * @return Time formatted
     */
    public void setTimeView(TextView timeView, TimeFormat timeFormat) {
        if (timeFormat == TimeFormat.TIME_FORMAT_HTML) {
            timeView.setText(getTimeHtmlFormat());
        } else if (timeFormat == TimeFormat.TIME_FORMAT_START_1_LINE) {
            timeView.setText(getTimeStartIn1Line());
        } else if (timeFormat == TimeFormat.TIME_FORMAT_START_2_LINE) {
            timeView.setText(getTimeStartIn2Line());
        }
    }

    private String getTimeStartIn2Line() {
        return DateUtil.parseStartDateIn2Line(mFromDate);
    }

    private String getTimeStartIn1Line() {
        return DateUtil.parseStartDateIn1Line(mFromDate);
    }

    /**
     * Get Html format String of duration time
     * @return
     */
    private Spanned getTimeHtmlFormat() {
        return DateUtil.parseTimeEvent(mFromDate, mEndDate);
    }

    public boolean isFavorite() {
        return "1".equals(mEventFavorite);
    }

    public void toggleFavorite() {
        if (isFavorite()) {
            mEventFavorite = "0";
            mFavoriteTotal--;
        } else {
            mEventFavorite = "1";
            mFavoriteTotal++;
        }
    }



    public boolean isNew() {
        try {
            return new Date().getTime() - DateUtil.parseInputDate(mDateCreated).getTime() <= NEW_TIME_EVENT;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

}
