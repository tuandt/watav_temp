package jp.co.aainc.watav.screen.login.model;

import com.google.gson.annotations.SerializedName;

import jp.co.aainc.watav.log.Logger;

/**
 * Created by Admin on 11/20/2014.
 */
public class LoginData {
    @SerializedName("uid")
    private String mUid;

    @SerializedName("access_token")
    private String mAccessToken;

    @SerializedName("name")
    private String mName;

    @SerializedName("profile_image_url")
    private String mProfileImageUrl;

    /**
     * Return login access token
     *
     * @return Access token
     */
    public String getAccessToken() {
        Logger.e("AT = " + mAccessToken);
        return mAccessToken;
    }
}
