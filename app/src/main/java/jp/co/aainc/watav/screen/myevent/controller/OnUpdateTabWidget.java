package jp.co.aainc.watav.screen.myevent.controller;

/**
 * Update tab widget listener
 * Created by TuanDT on 12/8/2014.
 */
public interface OnUpdateTabWidget {
    void updateTabWidget(int eventsCount);
}
