package jp.co.aainc.watav.common.pref;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Set;

/**
 * Manage App settings stored in {@link android.content.SharedPreferences}
 * Created by Tuandt on 11/14/2014.
 */
public class PrefManager {
    private  static final String PREF_FILE = "watav_pref";
    private  static final String ACCESS_TOKEN = "access_token";

    /**
     * Logged in when has Access token saved in Preference
     * @return
     */
    public static boolean isLoggedIn(Context context) {
        return getAccessToken(context) != null;
    }

    /**
     * Save Access token to {@link android.content.SharedPreferences}
     * @param context Context provide SharePreference
     * @param accessToken to be saved
     */
    public static void saveAccessToken(Context context, String accessToken) {
        saveSetting(context, ACCESS_TOKEN, accessToken);
    }

    public static String getAccessToken(Context context) {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE).getString(ACCESS_TOKEN, null);
    }

    private static void saveSetting(Context context, String key, String value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putString(key, value);
        editor.commit();
    }

    private static void saveSetting(Context context, String key, int value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putInt(key, value);
        editor.commit();
    }

    private static void saveSetting(Context context, String key, boolean value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putBoolean(key, value);
        editor.commit();
    }

    private static void saveSetting(Context context, String key, float value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putFloat(key, value);
        editor.commit();
    }

    private static void saveSetting(Context context, String key, long value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putLong(key, value);
        editor.commit();
    }

    private static void saveSetting(Context context, String key, Set<String> value) {
        SharedPreferences pref = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        editor.putStringSet(key, value);
        editor.commit();
    }
}
