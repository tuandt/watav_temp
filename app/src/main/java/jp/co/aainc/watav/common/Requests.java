package jp.co.aainc.watav.common;

/**
 * App request constants
 * Created by Admin on 11/13/2014.
 */
public interface Requests {
    public static final String BASE_URL = "http://watav.aa-dev.com/watav/";

    public interface Methods {
        public static final String LOGIN = BASE_URL + "login.json";
        public static final String RECOMMEND = BASE_URL + "list_event_for_recomment.json";
        public static final String SUGGEST = BASE_URL + "list_suggest_event.json";
        public static final String RANKING = BASE_URL + "list_event_by_ranking.json";
        public static final String LIST_EVENT = BASE_URL + "list_event.json";
        public static final String LIST_FB_EVENT = BASE_URL + "list_fb_event.json";
        public static final String REGISTER_EVENT_STATUS = BASE_URL + "regist_event_status.json";
        public static final String CATEGORIES = BASE_URL + "list_category.json";
    }

    public interface Params {
        public static final String LOGIN_UID = "login_uid";
        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String MAIL_ADDRESS = "mail_address";
        public static final String PROFILE_IMG_URL = "profile_img_url";
        public static final String GENDER = "gender";
        public static final String BIRTH_DAY = "birth_day";
        public static final String USER_TYPE = "user_type";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String DEVICE_ID = "device_id";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String TOKEN = "token";
        public static final String EVENT_ID = "event_id";
        public static final String EVENT_FAVORITE = "event_favorite";
        public static final String PAGE = "page";
        public static final String COUNT = "count";
        public static final String SORT = "sort";
        public static final String ORDER = "order";
        public static final String START_DATE_FR = "start_date_fr";
        public static final String START_DATE_TO = "start_date_to";
        public static final String END_DATE_FR = "end_date_fr";
        public static final String END_DATE_TO = "end_date_to";
        public static final String JOIN_OR_END_FR = "join_or_end_fr";
        public static final String JOIN_OR_END_TO = "join_or_end_to";
        public static final String EVENT_HIDDEN = "event_hidden";
        public static final String CATEGORY_ID = "category_id";
        public static final String EVENT_STATUS = "event_status";
    }

    public interface Values {
        public static final String SORT_FROM_DATE = "from_date";
        public static final String SORT_END_DATE = "end_date";
        public static final String SORT_DATE_CREATED = "date_created";
        public static final String ORDER_DESC = "desc";
        public static final String ORDER_ASC = "asc";
    }
}
