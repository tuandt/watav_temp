package jp.co.aainc.watav.screen.login.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Login Activity
 * Created by TuanDT on 12/11/2014.
 */
public class LoginActivity extends FragmentActivity implements LoginFragment.OnLoginSuccessListener{
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_login);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.login_frame, new LoginFragment());
        transaction.commit();
    }

    @Override
    public void onLoggedIn() {
        Logger.e("onLoggedIn AT=" + PrefManager.getAccessToken(this));
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
