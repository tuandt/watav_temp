package jp.co.aainc.watav.screen.main.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.log.Logger;

/**
 * Base Main Fragments
 * Created by TuanDT on 12/11/2014.
 */
public abstract class MainFragment extends BaseFragment {
    protected static final int ID_ROOT_FRAME = R.id.main_root_frame;
    private boolean mIsInitialized = false;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    /**
     * Add fragment
     *
     * @param canBack  Can press Back key on fragment to return previous fragment
     * @param fragment Fragment view
     */
    protected void addFragment(Fragment fragment, boolean canBack) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();

        transaction.add(ID_ROOT_FRAME, fragment);
        if (canBack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Logger.e("MainFragment onHiddenChanged " +hidden);
        if (!hidden && !mIsInitialized) {
            Fragment f = onCreateFirstFragment();
            addFragment(f, false);
            f.onHiddenChanged(hidden);
            mIsInitialized = true;
        }
    }

    protected abstract Fragment onCreateFirstFragment();
}
