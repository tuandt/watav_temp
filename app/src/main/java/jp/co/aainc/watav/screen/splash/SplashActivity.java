package jp.co.aainc.watav.screen.splash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.RelativeLayout;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.screen.login.controller.LoginActivity;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Created by TuanDT on 11/12/2014.
 */
public class SplashActivity extends Activity {
    private RelativeLayout mBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        mBackground = (RelativeLayout) findViewById(R.id.bg_splash_layout);
        mBackground.setBackgroundResource(R.drawable.bg_splash);

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

//
//                Intent i = new Intent(getApplication(), MainActivity.class);
//                startActivity(i);
//                finish();
                checkLogin();
            }
        }).start();
    }

    /**
     * Check login status
     */
    private void checkLogin() {
        if (PrefManager.isLoggedIn(this)) {
            startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
        finish();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBackground.setBackground(null);
    }
}
