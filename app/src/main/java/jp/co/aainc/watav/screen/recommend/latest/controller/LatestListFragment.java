package jp.co.aainc.watav.screen.recommend.latest.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;

/**
 * Suggest events list fragment
 * Created by TuanDT on 11/27/2014.
 */
public class LatestListFragment extends ListEventsFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.LIST_EVENT;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_list_item_common;
    }

    @Override
    protected boolean loadBlurCover() {
        return true;
    }

    @Override
    protected Event.TimeFormat getTimeFormat() {
        return Event.TimeFormat.TIME_FORMAT_HTML;
    }

    @Override
    protected Map<String, String> getParams(Activity activity) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(activity));
        params.put(Requests.Params.SORT, Requests.Values.SORT_DATE_CREATED);
        params.put(Requests.Params.ORDER, Requests.Values.ORDER_DESC);
        params.put(Requests.Params.EVENT_HIDDEN, "0");
        return params;
    }
}
