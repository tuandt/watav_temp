package jp.co.aainc.watav.screen.ranking.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.log.Logger;

/**
 * Created by Admin on 12/2/2014.
 */
public class DummyFragment extends BaseFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        TextView view = new TextView(getActivity());
        view.setText("Dummy");
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                Logger.e("Dummy Touch");
                return false;
            }
        });
        return view;
    }
}
