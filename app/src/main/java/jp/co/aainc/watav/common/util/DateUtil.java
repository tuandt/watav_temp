package jp.co.aainc.watav.common.util;


import android.text.Html;
import android.text.Spanned;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Date, time & Calendar utilities
 * Created by TuanDT on 11/21/2014.
 */
public class DateUtil {
    private static final String FORMAT_INPUT = "yyyy-MM-dd HH:mm:ss";
    private static final String FORMAT_OUTPUT = "yyyy.MM.dd (E) HH:mm";
    private static final String FORMAT_START_OUTPUT_2_LINE = "yyyy.MM.dd (E)\nHH:mm~";
    private static final String FORMAT_START_OUTPUT_1_LINE = "yyyy.MM.dd (E) HH:mm~";
    private static final String TIME_EVENT_FORMAT = "<html><body>" +
            "<big><b>%1s</b></big>" +
            "%2s<br>" +
            "<small>%3s</small>" +
            "</body></html>";

    private static final SimpleDateFormat INPUT_FORMAT = new SimpleDateFormat(FORMAT_INPUT, Locale.JAPAN);

    /**
     * Parse input date from string
     * @param inputDate input date String from server
     * @return Date parsed
     */
    public static Date parseInputDate(String inputDate) throws ParseException{
        return INPUT_FORMAT.parse(inputDate);
    }

    /**
     * Parse Date time
     * @param inputDate input date String from server
     * @return Output date String to view
     */
    public static String parseDate(String inputDate) {
        String outputDate = null;
        SimpleDateFormat outputFormat = new SimpleDateFormat(FORMAT_OUTPUT, Locale.JAPAN);

        try {
            Date date = parseInputDate(inputDate);
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    /**
     * Parse Start Date time into 2 line
     * @param inputDate input date String from server
     * @return Output date String to view
     */
    public static String parseStartDateIn2Line(String inputDate) {
        String outputDate = null;
        SimpleDateFormat outputFormat = new SimpleDateFormat(FORMAT_START_OUTPUT_2_LINE, Locale.JAPAN);

        try {
            Date date = parseInputDate(inputDate);
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    /**
     * Parse Start Date time into 1 line
     * @param inputDate input date String from server
     * @return Output date String to view
     */
    public static String parseStartDateIn1Line(String inputDate) {
        String outputDate = null;
        SimpleDateFormat outputFormat = new SimpleDateFormat(FORMAT_START_OUTPUT_1_LINE, Locale.JAPAN);

        try {
            Date date = parseInputDate(inputDate);
            outputDate = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return outputDate;
    }

    /**
     * Parse time event to Html format
     * @param start Start date
     * @param end End date
     * @return Html text
     */
    public static Spanned parseTimeEvent(String start, String end) {
        try {
            // Parse start time
            Date startDate = parseInputDate(start);
            String startDay = new SimpleDateFormat("yyyy.MM.dd", Locale.JAPAN).format(startDate);
            String startTime = new SimpleDateFormat("(E) HH:mm~", Locale.JAPAN).format(startDate);

            // Parse end time
            String endDateTime = "";
            if (end != null) {
                Date endDate = parseInputDate(end);
                endDateTime = new SimpleDateFormat("yyyy.MM.dd (E) HH:mm", Locale.JAPAN).format(endDate);
            }

            // Return formatted data
            return Html.fromHtml(String.format(TIME_EVENT_FORMAT, startDay, startTime, endDateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Parse Date with default format
     * @param date Input Date
     * @return Date on string
     */
    public static String parseDate(Date date) {
        return INPUT_FORMAT.format(date);
    }

    /**
     * Parse current time with default format
     * @return String of current time
     */
    public static String parseCurrentTime() {
        return parseDate(new Date());
    }

    /**
     * Parse Date with default format, Tokyo time zone
     * @param date Input Date
     * @return Date on string
     */
    public static String parseJapanDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_INPUT);
        TimeZone tz = TimeZone.getTimeZone("Asia/Tokyo");
        format.setTimeZone(tz);
        return  format.format(date);
    }

    /**
     * Parse current time with default format and set to Tokyo Time zone
     * @return String of current time
     */
    public static String parseJapanCurrentTime() {
        return parseJapanDate(new Date());
    }

    /**
     * Parse current time without hour, minute an seconds
     * @return Date parsed
     */
    public static String parseCurrentTimeWithoutHour() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + " 00:00:00";
    }
}
