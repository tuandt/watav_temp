package jp.co.aainc.watav.common;

/**
 * App constants
 * Created by Admin on 11/14/2014.
 */
public interface Constants {
    public static final String FACEBOOK_IMAGE_URL = "https://graph.facebook.com/%1s/picture";
}
