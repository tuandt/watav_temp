package jp.co.aainc.watav.common.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.recommend.controller.OnHorizontalScrollListener;

/**
 * Custom {@link android.support.v4.view.ViewPager}
 * Created by TuanDT on 12/1/2014.
 */
public class CustomViewPager extends ViewPager implements View.OnTouchListener {
    private boolean mCanScroll = true;
    private OnHorizontalScrollListener mListener;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
//        dispatchAllChildren(ev);
        if (!mCanScroll) {
            return false;
        }

        return super.onInterceptTouchEvent(ev);
    }

    public void setCanScroll(boolean canScroll) {
        mCanScroll = canScroll;
    }

//    @Override
//    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
//        Logger.e("canScroll " + mCanScroll);
//        if (!mCanScroll) {
//            return false;
//        }
//        return super.canScroll(v, checkV, dx, x, y);
//    }

    /**
     * Set Horizontal scroll listener
     * @param listener
     */
    public void setOnHorizontalScrollListener(OnHorizontalScrollListener listener) {
        mListener = listener;
        this.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        Logger.e("onTouch " + this.getCurrentItem());
        if (mListener != null) {
            if (this.getCurrentItem() != 0) {
                mListener.onAtMiddlePosition();
            } else {
                mListener.onAtStartPosition();
            }
        }
        return false;
    }
}
