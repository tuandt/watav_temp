package jp.co.aainc.watav.common.view;

import android.content.Context;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Custom Sliding menu
 * Created by TuanDT on 11/25/2014.
 */
public class CustomSlidingPanelLayout extends SlidingPaneLayout {
    private boolean mCanSlide;

    public CustomSlidingPanelLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mCanSlide = true;
    }

    public void setCanSlide(boolean canSlide) {
        mCanSlide = canSlide;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if (!mCanSlide && !this.isOpen()) {
            // Dispatch event to Content layout (Child layout at index 1)
            getChildAt(1).dispatchTouchEvent(ev);
            return false;
        }
        return super.onInterceptTouchEvent(ev);
    }
}
