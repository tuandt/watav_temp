package jp.co.aainc.watav.common.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.model.Pager;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.common.util.NetworkUtil;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.request.manager.BaseRequest;
import jp.co.aainc.watav.screen.myevent.controller.OnUpdateTabWidget;

/**
 * Common List events
 * Created by TuanDT 11/26/2014.
 */
public abstract class ListEventsFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String KEY_INITIAL_ON_CREATE = "key_initial_on_create";
    protected static final int PAGE_COUNT = 10;
    private boolean mIsInitialized = false;

    // Current page
    private int mCurrentPage;

    // Pager
    private Pager mPager;

    // Refresh widget
    private SwipeRefreshLayout mSwipeRefreshWidget;

    // List view
    protected RecyclerView mRecycleView;

    // List adapter
    private BaseAdapter mAdapter;

    // Events
    private List<Event> mEvents;

    // Loading data
    private boolean mIsLoading;

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_events, container, false);
        mIsLoading = false;
        mActivity = getActivity();

        // Refresh widget
        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorSchemeResources(R.color.refresh_color1, R.color.refresh_color2,
                R.color.refresh_color3, R.color.refresh_color4);
        mSwipeRefreshWidget.setOnRefreshListener(this);

        // Recycle list view
        mRecycleView = (RecyclerView) view.findViewById(R.id.list_events_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecycleView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        mRecycleView.setLayoutManager(layoutManager);

        // Handle Load more
        mRecycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager manager = (LinearLayoutManager) mRecycleView.getLayoutManager();
                int lastVisible = manager.findLastCompletelyVisibleItemPosition();
                int eventCount = mAdapter.getEventsCount();

                if (!mIsLoading && lastVisible >= eventCount - 1
                        && eventCount < mPager.getTotalCount()) {
                    mIsLoading = true;
                    onLoadMore();
                }
            }
        });

        // init an adapter with empty
        mEvents = new ArrayList<>();
        mAdapter = new ListEventsAdapter(mActivity, mEvents, getLayoutResourceId(), getTimeFormat(), loadBlurCover());
        mRecycleView.setAdapter(mAdapter);

        // Initial data?
        Bundle args = getArguments();
        if (args == null || args.getBoolean(KEY_INITIAL_ON_CREATE, true))
            initData(mActivity);

        return view;
    }

    /**
     * Initialize data
     */
    public void initData(Activity activity) {
        if (!mIsInitialized) {
            mIsInitialized = true;
            DialogUtil.showProgressDialog(mActivity);
            mCurrentPage = 1;
            requestData(activity, 1, PAGE_COUNT, mRefreshListener);
        }
    }

    @Override
    public void onRefresh() {
        Logger.e("onRefresh " + mRecycleView.getLayoutManager().getItemCount());
        mSwipeRefreshWidget.setRefreshing(true);
        requestData(mActivity, 1, mCurrentPage * PAGE_COUNT, mRefreshListener);
    }

    /**
     * Load more list
     */
    protected void onLoadMore() {
        Logger.e("onLoadMore " + mRecycleView.getLayoutManager().getItemCount());
        DialogUtil.showProgressDialog(mActivity);
        mCurrentPage++;
        requestData(mActivity, mCurrentPage, PAGE_COUNT, mLoadMoreListener);
    }

    /**
     * Load data from server
     *
     * @param page Page to load
     */
    private void requestData(Activity activity, int page, int count, RequestListener requestListener) {
        mIsLoading = true;
        if (NetworkUtil.checkNetwork(activity)) {
            Map<String, String> params = getParams(activity);
            params.put(Requests.Params.PAGE, String.valueOf(page));
            params.put(Requests.Params.COUNT, String.valueOf(count));
            new ListRequest(ListData.class, getUrl(), params, requestListener).send();
        }
    }

    /**
     * Url request of extend fragment
     *
     * @return Url
     */
    protected abstract String getUrl();

    /**
     * Init list item layout resource id
     *
     * @return Layout id
     */
    protected abstract int getLayoutResourceId();

    /**
     * Cover will be loaded as clear image or blur one
     *
     * @return blur option
     */
    protected abstract boolean loadBlurCover();

    /**
     * Text time format
     *
     * @return Format
     */
    protected abstract Event.TimeFormat getTimeFormat();

    /**
     * Request Parameters of extend fragment
     *
     * @return Params
     */
    protected abstract Map<String, String> getParams(Activity activity);

    // Request response listener
    private RequestListener mRefreshListener = new RequestListener<ListData>() {
        @Override
        public void onSuccess(ListData data) {
            mPager = data.getPager();
            List<Event> events = data.getEvents();

            // Add all events
            mEvents.clear();
            mEvents.addAll(events);


            boolean canLoadMore = (events.size() < mPager.getTotalCount());
            mAdapter.setCanLoadMore(canLoadMore);

            // Update tab widget if needed
            Fragment parent = getParentFragment();
            if (parent != null && parent instanceof OnUpdateTabWidget) {
                ((OnUpdateTabWidget) parent).updateTabWidget(mPager.getTotalCount());
            }

            // Finish refresh
            mAdapter.notifyDataSetChanged();
            mSwipeRefreshWidget.setRefreshing(false);
            mIsLoading = false;
        }
    };

    // Request response listener
    private RequestListener mLoadMoreListener = new RequestListener<ListData>() {
        @Override
        public void onSuccess(ListData data) {
            mPager = data.getPager();
            mEvents.addAll(data.getEvents());

            // Check load more
            boolean canLoadMore = (mAdapter.getEventsCount() < mPager.getTotalCount());
            mAdapter.setCanLoadMore(canLoadMore);

            // Notify & finish load more
            mAdapter.notifyDataSetChanged();
            mIsLoading = false;
            mSwipeRefreshWidget.setRefreshing(false);
        }
    };

    /**
     * Common request for list data
     */
    private class ListRequest extends BaseRequest<ListData> {
        private final String TAG = ListRequest.class.getSimpleName();
        private final String mUrl;

        public ListRequest(Class<ListData> clazz, String url, Map<String, String> params, BaseListener<ListData> listener) {
            super(clazz, params, listener);
            mUrl = url;
        }

        @Override
        protected String getUrl() {
            return mUrl;
        }

        @Override
        protected int getMethod() {
            return Request.Method.GET;
        }

        @Override
        protected String getTag() {
            return TAG;
        }
    }

    /**
     * Common list data
     */
    private class ListData {
        @SerializedName("results")
        private List<Event> mEvents;

        @SerializedName("pager")
        private Pager mPager;

        public List<Event> getEvents() {
            return mEvents;
        }

        public Pager getPager() {
            return mPager;
        }
    }
}