package jp.co.aainc.watav.common.controller;

import android.support.v4.app.Fragment;

import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.request.manager.BaseRequest;

/**
 * Base Fragments
 * Created by TuanDT on 11/18/2014.
 */
public abstract class BaseFragment extends Fragment {

    /**
     * Common listener with common error handler
     */
    protected abstract class RequestListener<DATA> implements BaseRequest.BaseListener<DATA> {

        @Override
        public void onError(int errorCode, String errorMsg) {
            DialogUtil.dismissProgressDialog(getActivity());

            if (errorCode == BaseRequest.ERROR_CODE_COMMON || errorMsg == null || errorMsg.equals("")) {
                DialogUtil.showRequestErrorDialog(getActivity());
            } else {
                DialogUtil.showDialogMessage(getActivity(), "Error " + errorCode, errorMsg);
            }
        }

        @Override
        public void onRetrievedData(DATA data) {
            DialogUtil.dismissProgressDialog(getActivity());

            // Check data null
            if (data != null) {
                onSuccess(data);
            } else {
                DialogUtil.showRequestErrorDialog(getActivity());
            }
        }

        public abstract void onSuccess(DATA data);
    }
}
