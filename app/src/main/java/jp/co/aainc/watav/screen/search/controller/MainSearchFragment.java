package jp.co.aainc.watav.screen.search.controller;

import android.support.v4.app.Fragment;

import jp.co.aainc.watav.screen.main.controller.MainFragment;

/**
 * Main Fragment controller for search screen
 * Created by TuanDT on 12/11/2014.
 */
public class MainSearchFragment extends MainFragment {
    @Override
    protected Fragment onCreateFirstFragment() {
        return new SearchFragment();
    }
}
