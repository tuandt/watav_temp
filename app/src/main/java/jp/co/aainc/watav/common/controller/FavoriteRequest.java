package jp.co.aainc.watav.common.controller;

import com.android.volley.Request;

import java.util.Map;

import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.request.manager.BaseRequest;

/**
 * Favorite & un-favorite event
 * Created by TuanDT on 11/24/2014.
 */
public class FavoriteRequest extends BaseRequest<Object> {
    private static final String TAG = FavoriteRequest.class.getSimpleName();

    public FavoriteRequest(Class<Object> clazz, Map<String, String> params, BaseListener<Object> listener) {
        super(clazz, params, listener);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.REGISTER_EVENT_STATUS;
    }

    @Override
    protected int getMethod() {
        return Request.Method.POST;
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}
