package jp.co.aainc.watav.screen.recommend.controller;

import android.support.v4.app.Fragment;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.screen.main.controller.MainFragment;
import jp.co.aainc.watav.screen.recommend.latest.controller.LatestListFragment;
import jp.co.aainc.watav.screen.recommend.suggest.controller.SuggestListFragment;

/**
 * Main Fragment controller for recommend screen
 * Created by TuanDT on 12/11/2014.
 */
public class MainRecommendFragment extends MainFragment implements OnShowAllClicked {
    @Override
    protected Fragment onCreateFirstFragment() {
        return new RecommendFragment();
    }

    @Override
    public void onShowAllClicked(String title) {
        if (title.equals(getString(R.string.title_recommend_staff))) {
            addFragment(new SuggestListFragment(), true);
        } else if (title.equals(getString(R.string.title_latest))) {
            addFragment(new LatestListFragment(), true);
        }
    }
}
