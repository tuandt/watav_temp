package jp.co.aainc.watav.screen.ranking.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DateUtil;

/**
 * Ranking screen
 * Created by TuanDT on 12/1/2014.
 */
public class RankingFragment extends ListEventsFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.RANKING;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.layout_list_item_ranking;
    }

    @Override
    protected boolean loadBlurCover() {
        return false;
    }

    @Override
    protected Event.TimeFormat getTimeFormat() {
        return Event.TimeFormat.TIME_FORMAT_START_1_LINE;
    }

    @Override
    protected Map<String, String> getParams(Activity activity) {
        Map<String, String> params = new HashMap<String, String>();
        params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(activity));
        params.put(Requests.Params.START_DATE_FR, DateUtil.parseCurrentTimeWithoutHour());

        return params;
    }
}