package jp.co.aainc.watav.common.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.request.manager.BaseRequest;

/**
 * Favorite button clicked
 * Created by TuanDT on 11/25/2014.
 */
public class OnFavoriteClickListener implements View.OnClickListener {
    private Event mEvent;
    private int mPosition;
    private Context mContext;
    private OnFavoriteRequestSuccess mOnRequestSuccess;
    private RecyclerView.Adapter mAdapter;

    public OnFavoriteClickListener(Context context, Event event, OnFavoriteRequestSuccess onFavoriteRequestSuccess) {
        mContext = context;
        mEvent = event;
        mOnRequestSuccess = onFavoriteRequestSuccess;
    }

    public OnFavoriteClickListener(Context context, BaseAdapter adapter, int position, OnFavoriteRequestSuccess onFavoriteRequestSuccess) {
//        mEvent = event;
        mPosition = position;
        mContext = context;
        mOnRequestSuccess = onFavoriteRequestSuccess;
        mAdapter = adapter;
        mPosition = position;
        mEvent = adapter.getItem(position);
    }

    @Override
    public void onClick(View view) {
        String favorite = mEvent.isFavorite() ? "0" : "1";
        Map<String, String> params = new HashMap<>();
        params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(mContext));
        params.put(Requests.Params.EVENT_ID, String.valueOf(mEvent.getId()));
        params.put(Requests.Params.EVENT_FAVORITE, favorite);
        new FavoriteRequest(Object.class, params, new FavoriteListener(mEvent)).send();
    }

    /**
     * Set favorite status request listener
     */
    private class FavoriteListener implements BaseRequest.BaseListener<Object> {
        private Event mEvent;

        private FavoriteListener(Event event) {
            mEvent = event;
        }

        @Override
        public void onError(int errorCode, String errorMsg) {
            DialogUtil.showDialogMessage(mContext, "Error " + errorCode, errorMsg);
            if (mAdapter != null) {
//            mAdapter.notifyPagerDataSetChanged();
                mAdapter.notifyItemChanged(mPosition);
            }
        }

        @Override
        public void onRetrievedData(Object data) {
            Toast.makeText(mContext, "Success", Toast.LENGTH_SHORT).show();
            mEvent.toggleFavorite();
            if (mOnRequestSuccess != null)
                mOnRequestSuccess.onSuccess();
            if (mAdapter != null) {
//                mAdapter.notifyPagerDataSetChanged();
                mAdapter.notifyItemChanged(mPosition);
            }
        }
    }

    public interface OnFavoriteRequestSuccess {
        public void onSuccess();
    }
}
