package jp.co.aainc.watav;

import android.app.Application;
import android.graphics.Bitmap;


import jp.co.aainc.watav.request.manager.RequestQueueManager;

/**
 * Created by Admin on 11/12/2014.
 */
public class App extends Application {
    public static final String TAG = App.class.getSimpleName();
    private static int DISK_IMAGE_CACHE_SIZE = 1024 * 1024 * 10;
    private static Bitmap.CompressFormat DISK_IMAGE_CACHE_COMPRESS_FORMAT = Bitmap.CompressFormat.PNG;
    private static int DISK_IMAGE_CACHE_QUALITY = 100;

    @Override
    public void onCreate() {
        super.onCreate();
        initVolley();
    }

    // TODO : ==== Volley ======
    /**
     * Initialize the request manager and the image cache
     */
    private void initVolley() {
        RequestQueueManager.init(this);
//        createImageCache();
    }

    /**
     * Create the image cache. Uses Memory Cache by default. Change to Disk for
     * a Disk based LRU implementation.
     */
//    private void createImageCache() {
//        ImageCacheManager.getInstance().init(this, this.getPackageCodePath(),
//                DISK_IMAGE_CACHE_SIZE, DISK_IMAGE_CACHE_COMPRESS_FORMAT,
//                DISK_IMAGE_CACHE_QUALITY, ImageCacheManager.CacheType.DISK);
//    }
}
