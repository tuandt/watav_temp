package jp.co.aainc.watav.screen.recommend.controller;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.BaseAdapter;
import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.common.controller.OnFavoriteClickListener;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.util.ImageUtil;
import jp.co.aainc.watav.common.view.CustomRecyclerView;
import jp.co.aainc.watav.log.Logger;

/**
 * Suggest events fragment,
 * Created by TuanDT on 11/24/2014.
 */
public class SuggestEventsFragment extends BaseFragment {
    private static final String KEY_TITLE = "title";
    private static final String KEY_NEW_EVENT = "isNewEvent";

    // Fragment title
    private String mTitle;

    // Events list
    private final List<Event> mEvents = new ArrayList<>();

    // Recycle View controllers
    private CustomRecyclerView mRecycleView;
    private RecyclerView.Adapter mAdapter;

    // Show new icon
    private boolean mIsShowNewIcon;

    // Holder activity
    private Activity mActivity;

    // RecyclerView scroll callback
    private OnHorizontalScrollListener mOnHorizontalScrollListener;

    // Show all button clicked listener
    private OnShowAllClicked mOnShowAllClicked;

    /**
     * Create a new instance of Fragment. Initialize with title
     *
     * @param title Fragment title
     */
    public static SuggestEventsFragment newInstance(String title, boolean isNewEvents) {
        SuggestEventsFragment fragment = new SuggestEventsFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TITLE, title);
        args.putBoolean(KEY_NEW_EVENT, isNewEvents);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommend_events, container, false);
        mActivity = getParentFragment().getActivity();

        // Set Title
        mTitle = getArguments().getString(KEY_TITLE);
        ((TextView) view.findViewById(R.id.recommend_events_title_tv)).setText(mTitle);

        mRecycleView = (CustomRecyclerView) view.findViewById(R.id.recommend_events_recycle_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecycleView.setHasFixedSize(true);

        // use a linear layout manager
        MyLinearLayoutManager layoutManager = new MyLinearLayoutManager(mActivity);
        mRecycleView.setLayoutManager(layoutManager);

        // specify an adapter
        mAdapter = new EventsAdapter(mActivity, mEvents);
        mRecycleView.setAdapter(mAdapter);

        // New events list?
        mIsShowNewIcon = getArguments().getBoolean(KEY_NEW_EVENT);

        // Show all text click listener
        view.findViewById(R.id.recommend_events_show_all_tv).setOnClickListener(new OnShowAllListener(mTitle));

        // Handle scrolling of RecyclerView
        mRecycleView.setOnHorizontalScrollListener(mOnHorizontalScrollListener);
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mOnShowAllClicked = getOnShowAllListener();
    }

    /**
     * Find parents handle button Show all click listener
     *
     * @return OnShowAllClicked
     */
    private OnShowAllClicked getOnShowAllListener() {
        Fragment parent = getParentFragment();

        while (parent != null) {
            Logger.e("SuggestEventsFragment onAttach " + parent);

            if (parent instanceof OnShowAllClicked) {
                return (OnShowAllClicked) parent;
            } else {
                parent = parent.getParentFragment();
            }
        }

        return null;
    }

    /**
     * Set events to be loaded
     *
     * @param events {@link jp.co.aainc.watav.common.model.Event}
     */
    public void setEvents(List<Event> events) {
        mEvents.clear();
        mEvents.addAll(events);
        mAdapter.notifyDataSetChanged();
    }

    public class EventsAdapter extends BaseAdapter<ViewHolder> {
        public EventsAdapter(Context context, List<Event> events) {
            super(context, events);
        }

        // Create new views (invoked by the layout manager)
        @Override
        public ViewHolder onCreateViewHolder(
                ViewGroup parent, int viewType) {
            // create a new view
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.layout_list_item_recommend, parent, false);
            // set the view's size, margins, padding and layout parameters if needed
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Event event = mEvents.get(position);

            // Load event data to views
            ImageUtil.loadImageByGlide(mActivity, holder.mCoverView, event.getImageUrl());
            holder.mTitleView.setText(event.getTitle());
            event.setTimeView(holder.mTimeView, Event.TimeFormat.TIME_FORMAT_START_2_LINE);

            int visibility = mIsShowNewIcon || event.isNew() ? View.VISIBLE : View.GONE;
            holder.mNewIcon.setVisibility(visibility);

            final boolean isFavorited = event.isFavorite();
            holder.mFavoriteCb.setChecked(isFavorited);
            holder.mFavoriteCb.setOnClickListener(new OnFavoriteClickListener(mActivity, this, position, null));
        }

        // Return the size of your data set (invoked by the layout manager)
        @Override
        public int getItemCount() {
            return mEvents.size();
        }
    }

    /**
     * Custom Layout manager to display Wrap contents
     */
    public static class MyLinearLayoutManager extends LinearLayoutManager {

        public MyLinearLayoutManager(Context context) {
            super(context, LinearLayoutManager.HORIZONTAL, false);
        }

        private int[] mMeasuredDimension = new int[2];

        @Override
        public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state,
                              int widthSpec, int heightSpec) {
            if (state.getItemCount() == 0) {
                setMeasuredDimension(widthSpec, heightSpec);
                return;
            }

            final int widthMode = View.MeasureSpec.getMode(widthSpec);
            final int heightMode = View.MeasureSpec.getMode(heightSpec);
            final int widthSize = View.MeasureSpec.getSize(widthSpec);
            final int heightSize = View.MeasureSpec.getSize(heightSpec);

            measureScrapChild(recycler, 0,
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                    mMeasuredDimension);

            int width = mMeasuredDimension[0];
            int height = mMeasuredDimension[1];

            switch (widthMode) {
                case View.MeasureSpec.EXACTLY:
                case View.MeasureSpec.AT_MOST:
                    width = widthSize;
                    break;
                case View.MeasureSpec.UNSPECIFIED:
            }

            switch (heightMode) {
                case View.MeasureSpec.EXACTLY:
                case View.MeasureSpec.AT_MOST:
                    height = heightSize;
                    break;
                case View.MeasureSpec.UNSPECIFIED:
            }

            setMeasuredDimension(width, height);
        }

        private void measureScrapChild(RecyclerView.Recycler recycler, int position, int widthSpec,
                                       int heightSpec, int[] measuredDimension) {
            View view = recycler.getViewForPosition(position);
            if (view != null) {
                RecyclerView.LayoutParams p = (RecyclerView.LayoutParams) view.getLayoutParams();
                int childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec,
                        getPaddingLeft() + getPaddingRight(), p.width);
                int childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec,
                        getPaddingTop() + getPaddingBottom(), p.height);
                view.measure(childWidthSpec, childHeightSpec);
                measuredDimension[0] = view.getMeasuredWidth();
                measuredDimension[1] = view.getMeasuredHeight();
                recycler.recycleView(view);
            }
        }
    }

    // Provide a reference to the views for each data item
    private class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mCoverView;
        public CheckBox mFavoriteCb;
        public TextView mTitleView;
        public TextView mTimeView;
        private ImageView mNewIcon;

        public ViewHolder(View v) {
            super(v);

            // Get views
            mCoverView = (ImageView) v.findViewById(R.id.recommend_item_cover_iv);
            mFavoriteCb = (CheckBox) v.findViewById(R.id.recommend_item_favorite_cb);
            mTitleView = (TextView) v.findViewById(R.id.recommend_item_title_tv);
            mTimeView = (TextView) v.findViewById(R.id.recommend_item_time_tv);
            mNewIcon = (ImageView) v.findViewById(R.id.recommend_item_new_iv);
        }
    }

    /**
     * Handle Scroll on RecyclerView
     */
    private void setRecyclerViewScrollListener() {

//        mRecycleView.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                if (mOnHorizontalScrollListener == null) {
//                    return false;
//                }
//
//                LinearLayoutManager layoutManager = (LinearLayoutManager) mRecycleView.getLayoutManager();
//                int firstFullVisible = layoutManager.findFirstCompletelyVisibleItemPosition();
//                int lastFullVisible = layoutManager.findLastCompletelyVisibleItemPosition();
//                Logger.v("firstFullVisible=" + firstFullVisible);
//                Logger.v("lastFullVisible=" + lastFullVisible);
//
//                if (firstFullVisible == 0) {
//                    mOnHorizontalScrollListener.onAtStartPosition();
//                } else if (lastFullVisible == mEvents.size() - 1) {
//                    mOnHorizontalScrollListener.onAtEndPosition();
//                } else {
//                    mOnHorizontalScrollListener.onAtMiddlePosition();
//                }
//                return false;
//            }
//        });
//
//        mRecycleView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                if (mOnHorizontalScrollListener != null) {
//                    mOnHorizontalScrollListener.onScrolled();
//                }
//            }
//        });
    }

    public void setOnRecyclerViewScrollListener(OnHorizontalScrollListener onHorizontalScrollListener) {
        this.mOnHorizontalScrollListener = onHorizontalScrollListener;
    }

    /**
     * On Show all text clicked listener
     */
    private class OnShowAllListener implements View.OnClickListener {
        private String mTitle;

        private OnShowAllListener(String title) {
            mTitle = title;
        }

        @Override
        public void onClick(View view) {
            if (mOnShowAllClicked != null)
                mOnShowAllClicked.onShowAllClicked(mTitle);
        }
    }
}
