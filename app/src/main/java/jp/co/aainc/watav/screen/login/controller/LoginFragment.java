package jp.co.aainc.watav.screen.login.controller;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.common.Constants;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DeviceUtil;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.common.util.NetworkUtil;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.login.model.LoginData;


/**
 * {@link android.support.v4.app.Fragment} view
 * Login with Facebook account or login as guest
 * Created by TuanDT on 11/12/2014.
 */
public class LoginFragment extends BaseFragment {
    private static final String TAG = LoginFragment.class.getSimpleName();

    // Facebook permissions
    private static final List<String> REQUIRED_PERMISSIONS
            = Arrays.asList("public_profile", "email", "user_birthday",
            "user_photos", "rsvp_event", "user_events", "read_friendlists",
            "user_friends", "publish_actions");

    // Facebook UI helper
    private UiLifecycleHelper mUiHelper;

    // Logged in listener
    private OnLoginSuccessListener mLoginListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUiHelper = new UiLifecycleHelper(getActivity(), mFacebookStatusCallback);
        mUiHelper.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        initViews(view);
        return view;
    }

    /**
     * Initialize views in fragment
     *
     * @param root Fragment root view
     */
    private void initViews(View root) {
        // Facebook button click
        root.findViewById(R.id.login_facebook_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWithFacebook();
            }
        });

        // Guest button click
        root.findViewById(R.id.login_guest_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLoginListener.onLoggedIn();
//                getKeyHash();
            }
        });
    }

    /**
     * Login with Facebook account
     */
    private void loginWithFacebook() {
        // Open session
        Session session = Session.getActiveSession();

        if (!session.isOpened() && !session.isClosed()) {
            session.openForPublish(new Session.OpenRequest(this)
                    .setPermissions(REQUIRED_PERMISSIONS)
                    .setCallback(mFacebookStatusCallback));
        } else {
            Session.openActiveSession(getActivity(), this, true, mFacebookStatusCallback);
        }
    }

    /**
     * Facebook status change
     *
     * @param session Facebook Session
     * @param state   Facebook State
     */
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        Logger.e("!! ex==null? " + (exception == null));
        Logger.e("se opened = " + session.isOpened());
        Logger.e("st name=" + state.name());
        if (exception != null)
            Logger.e("ex " + exception.toString());
        if (state.isOpened()) {
            Logger.i("Logged in state..." + state.name());
            checkPermissions(session, state);
        } else if (state.isClosed()) {
            Logger.i("Logged out...");
        }
    }

    /**
     * Send login request to server
     *
     * @param params Login parameters
     */
    private void requestLogin(Map<String, String> params) {
        if (NetworkUtil.checkNetwork(getActivity())) {
            new LoginRequest(getActivity(), LoginData.class, params, mListener).send();
        }
    }

    /**
     * Check if all permissions not allowed
     *
     * @param session Login session
     */
    private void checkPermissions(Session session, SessionState state) {
        List<String> grantedPermissions = session.getPermissions();

        // If not all permissions is granted
        if (!grantedPermissions.containsAll(REQUIRED_PERMISSIONS)) {
            List<String> declinedPermissions = new ArrayList<>(REQUIRED_PERMISSIONS);
            declinedPermissions.removeAll(grantedPermissions);

            if (session.isOpened() && state == SessionState.OPENED) {
                Session.NewPermissionsRequest newPermissionsRequest = new Session
                        .NewPermissionsRequest(this, declinedPermissions);

                session.requestNewPublishPermissions(newPermissionsRequest);
            } else {
                session.refreshPermissions();
                session.closeAndClearTokenInformation();
            }
        } else {
            getFacebookUserInfo(session);
        }
    }

    /**
     * Get Facebook user information
     *
     * @param session Login session
     */
    private void getFacebookUserInfo(final Session session) {
        Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                // If the response is successful
                if (session == Session.getActiveSession() && user != null) {
                    // User info send to server
                    Map<String, String> info = new HashMap<String, String>();

                    String uid = user.getId();
                    info.put(Requests.Params.LOGIN_UID, uid);
                    info.put(Requests.Params.FIRST_NAME, user.getFirstName());
                    info.put(Requests.Params.LAST_NAME, user.getLastName());
                    info.put(Requests.Params.MAIL_ADDRESS, user.getProperty("email").toString());

                    String imgUrl = String.format(Constants.FACEBOOK_IMAGE_URL, uid);
                    info.put(Requests.Params.PROFILE_IMG_URL, imgUrl);
                    info.put(Requests.Params.GENDER, user.getProperty("gender").toString());
                    info.put(Requests.Params.BIRTH_DAY, user.getBirthday());
                    info.put(Requests.Params.USER_TYPE, "1");
                    info.put(Requests.Params.DEVICE_TOKEN, DeviceUtil.getDeviceId(getActivity()));
                    info.put(Requests.Params.ACCESS_TOKEN, session.getAccessToken());

                    requestLogin(info);
                } else {
                    DialogUtil.showRequestErrorDialog(getActivity());
                }
            }
        });
        Request.executeBatchAsync(request);
    }

    // Request response listener
    private RequestListener mListener = new RequestListener<LoginData>() {
        @Override
        public void onSuccess(LoginData data) {
            PrefManager.saveAccessToken(getActivity(), data.getAccessToken());
            mLoginListener.onLoggedIn();
        }
    };

    // Facebook status callback
    private Session.StatusCallback mFacebookStatusCallback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        mLoginListener = (OnLoginSuccessListener) a;
    }

    @Override
    public void onResume() {
        super.onResume();
        mUiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mUiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        mUiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mUiHelper.onSaveInstanceState(outState);
    }

    public interface OnLoginSuccessListener {
        public void onLoggedIn();
    }

    private void getKeyHash() {
        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    "jp.co.aainc.watav",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Logger.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
