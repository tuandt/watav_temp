package jp.co.aainc.watav.screen.myevent.controller;

/**
 * On history event page open & close listener
 * Created by TuanDT on 12/9/2014.
 */
public interface OnHistoryEventsListener {
    public void onHistoryOpen();
    public void onHistoryClose();
}
