package jp.co.aainc.watav.screen.myevent.history.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.controller.ListPagersAdapter;
import jp.co.aainc.watav.common.controller.PagersFragment;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.main.controller.MainActivity;
import jp.co.aainc.watav.screen.myevent.controller.MyEventPagersAdapter;
import jp.co.aainc.watav.screen.myevent.controller.OnHistoryEventsListener;
import jp.co.aainc.watav.screen.myevent.controller.OnUpdateTabWidget;

/**
 * My history Events fragment
 * Created by TuanDT on 12/3/2014.
 */
public class MyHistoryEventsFragment extends PagersFragment implements OnUpdateTabWidget {
    private static final String KEY_FORCE_INIT = "force_init";
    private boolean mCreated = false;

    /**
     * New instance of fragment
     * @param forceInitData Force init data on creation
     * @return MyHistoryEventsFragment
     */
    public static MyHistoryEventsFragment newInstance(boolean forceInitData) {
        MyHistoryEventsFragment fragment = new MyHistoryEventsFragment();
        Bundle args = new Bundle();
        args.putBoolean(KEY_FORCE_INIT, forceInitData);
        fragment.setArguments(args);
        return fragment;
    }

//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        Bundle args = getArguments();
//        if (args != null && args.getBoolean(KEY_FORCE_INIT, false)) {
//            onCreateData(mActivity);
//        }
//    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tabs_fixed;
    }

    @Override
    protected ListPagersAdapter createAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager viewPager) {
        return new MyEventPagersAdapter(activity, manager, tabHost, viewPager);
    }

    @Override
    protected void initializeData(Activity activity) {
        Bundle args = new Bundle();
        args.putBoolean(ListEventsFragment.KEY_INITIAL_ON_CREATE, false);

        addTab(getString(R.string.title_my_event_favorite), MyHistoryFavoritedEventsFragment.class, args, MyHistoryFavoritedEventsFragment.class.getName());
        addTab(getString(R.string.title_my_event_join), MyHistoryJoinedEventsFragment.class, args, MyHistoryJoinedEventsFragment.class.getName());
        addTab(getString(R.string.title_my_event_invited), MyHistoryInvitedEventsFragment.class, args, MyHistoryInvitedEventsFragment.class.getName());
        notifyDataSetChanged(true);
    }

    @Override
    public void updateTabWidget(int eventsCount) {
        TabWidget widget = mTabHost.getTabWidget();
        View view = widget.getChildTabViewAt(mTabHost.getCurrentTab());

        // Change badge text
        TextView badgeView = (TextView) view.findViewById(R.id.tab_badge_tv);
        badgeView.setText(String.valueOf(eventsCount));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getOnHistoryEventsListener().onHistoryClose();
    }

    /**
     * Find parents handle button History click listener
     *
     * @return OnShowAllClicked
     */
    private OnHistoryEventsListener getOnHistoryEventsListener() {
        Fragment parent = getParentFragment();

        while (parent != null) {
            Logger.e("MyEventsFragment onAttach " + parent);

            if (parent instanceof OnHistoryEventsListener) {
                return (OnHistoryEventsListener) parent;
            } else {
                parent = parent.getParentFragment();
            }
        }

        return null;
    }
}