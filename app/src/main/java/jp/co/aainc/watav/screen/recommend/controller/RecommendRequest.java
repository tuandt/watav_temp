package jp.co.aainc.watav.screen.recommend.controller;

import com.android.volley.Request;

import java.util.Map;

import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.request.manager.BaseRequest;
import jp.co.aainc.watav.screen.recommend.model.RecommendData;

/**
 * Request recommends data
 * Created by TuanDT on 11/18/2014.
 */
public class RecommendRequest extends BaseRequest<RecommendData> {
    private static final String TAG = RecommendRequest.class.getSimpleName();

    public RecommendRequest(Class<RecommendData> clazz, Map<String, String> params, BaseListener<RecommendData> listener) {
        super(clazz, params, listener);
    }

    @Override
    protected String getUrl() {
        return Requests.Methods.RECOMMEND;
    }

    @Override
    protected int getMethod() {
        return Request.Method.GET;
    }

    @Override
    protected String getTag() {
        return TAG;
    }
}

