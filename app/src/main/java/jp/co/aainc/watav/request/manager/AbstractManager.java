package jp.co.aainc.watav.request.manager;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractManager<T> {
	public static final String TAG = AbstractManager.class.getSimpleName();
	public void sendRequest(int method, String url, Class<T> clazz,
			Listener<T> listener, ErrorListener errorListener, String tag) {

        // Setting headers & params
		Map<String, String> headers = getHeaders();
		Map<String, String> params = getParameters();

//        if (headers == null) {
//            headers = new HashMap<String, String>();
//        }
//
//        if (params == null) {
//            params = new HashMap<String, String>();
//        }

        // Send request
		final GsonRequest<T> request = new GsonRequest<T>(method, url, clazz,
				headers, params, listener, errorListener);
		request.setTag(tag == null || tag.length() <= 0 ? TAG : tag);
		RequestQueueManager.getRequestQueue().add(request);
	}

	/**
	 * Cancel request with tag.
	 * @param tag Tag of request need to cancel
	 */
	public void cancelRequest(final String tag) {
		if (RequestQueueManager.getRequestQueue() != null) {
			RequestQueueManager.getRequestQueue().cancelAll(
					tag == null || tag.length() <= 0 ? TAG : tag);
		}
	}

	/**
	 * Get the header. The sub_class can override it.
	 * @return map.
	 */
	public Map<String, String> getHeaders() {
        return  new HashMap<String, String>();
    }

	/**
	 * Get the parameter, The sub_class can override it.
	 * @return map.
	 */
	public Map<String, String> getParameters() {
        return  new HashMap<String, String>();
    }
}
