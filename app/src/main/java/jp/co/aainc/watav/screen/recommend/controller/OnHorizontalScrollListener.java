package jp.co.aainc.watav.screen.recommend.controller;

/**
 * Handle horizontal scroll action
 * Created by TuanDT on 11/25/2014.
 */
public interface OnHorizontalScrollListener {
    public void onAtStartPosition();
    public void onAtMiddlePosition();
    public void onAtEndPosition();
    public void onScrolled();
}
