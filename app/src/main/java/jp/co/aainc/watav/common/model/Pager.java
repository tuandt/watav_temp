package jp.co.aainc.watav.common.model;

import com.google.gson.annotations.SerializedName;

/**
 * Pager data
 * Created by TuanDT on 11/27/2014.
 */
public class Pager {
    @SerializedName("total_count")
    private int mTotalCount;
    @SerializedName("page")
    private int mPage;
    @SerializedName("count")
    private int mCount;

    public int getTotalCount() {
        return mTotalCount;
    }

    public int getPage() {
        return mPage;
    }

    public int getCount() {
        return mCount;
    }
}
