package jp.co.aainc.watav.common.controller;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.util.ImageUtil;
import jp.co.aainc.watav.common.view.CategoriesLayout;

/**
 * Common list events adapter
 * Created by TuanDT on 11/28/2014.
 */
public class ListEventsAdapter extends BaseAdapter<RecyclerView.ViewHolder> {
    private int mLayoutResourceId;
    private Event.TimeFormat mTimeFormat;
    private boolean mLoadBlurCover;

    public ListEventsAdapter(Context context, List<Event> events,
                             int layoutResId, Event.TimeFormat timeFormat, boolean loadBlurCover) {
        super(context, events);
        mLayoutResourceId = layoutResId;
        mTimeFormat = timeFormat;
        mLoadBlurCover = loadBlurCover;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        //inflate your layout and pass it to view holder
        if (viewType == TYPE_ITEM) {
            // create a new view
            View v = inflater.inflate(mLayoutResourceId, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        } else {
            return getFooterHolder(inflater, parent);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_FOOTER) {
            return;
        }

        ViewHolder vh = (ViewHolder) holder;

        final Event event = getItem(position);

        // Position view
        if (vh.mPositionView != null) {
            vh.mPositionView.setText(String.valueOf(position + 1));
        }

        // Load event data to views
        vh.mTitleView.setText(event.getTitle());
        event.setTimeView(vh.mTimeView, mTimeFormat);

        if (event.isNew()) {
            vh.mNewIcon.setVisibility(View.VISIBLE);
        } else {
            vh.mNewIcon.setVisibility(View.GONE);
        }

        final boolean isFavorited = event.isFavorite();
        vh.mFavoriteCb.setChecked(isFavorited);

        Context context = getContext();
        vh.mFavoriteCb.setOnClickListener(new OnFavoriteClickListener(context, this, position, null));
        vh.mCategoriesLayout.setCategories(event.getCategories());

        int textColor = isFavorited ? context.getResources().getColor(R.color.favorite_enable_text) :
                context.getResources().getColor(R.color.favorite_disable_text);
        vh.mFavoriteCountTv.setTextColor(textColor);

        int favoriteCount = event.getFavoriteTotal();
        vh.mFavoriteCountTv.setText(String.valueOf(favoriteCount));
        if (favoriteCount == 0) {
            vh.mFavoriteCountTv.setVisibility(View.GONE);
        } else {
            vh.mFavoriteCountTv.setVisibility(View.VISIBLE);
        }

        if (mLoadBlurCover) {
            ImageUtil.loadImageWithGlideAndBlur(context, event.getImageUrl(), vh.mCoverImage);
//            ImageUtil.loadImageLoaderAndBlur(event.getImageUrl(), vh.mCoverImage);
//            ImageUtil.loadImageWithPicasso(context, event.getImageUrl(), vh.mCoverImage);
        } else {
            ImageUtil.loadImageByGlide(context, vh.mCoverImage, event.getImageUrl());
        }
    }

    // Provide a reference to the views for each data item
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mPositionView;
        public ImageView mCoverImage;
        public TextView mTitleView;
        public TextView mTimeView;
        private ImageView mNewIcon;
        private CategoriesLayout mCategoriesLayout;
        private TextView mFavoriteCountTv;
        private CheckBox mFavoriteCb;

        public ViewHolder(View v) {
            super(v);

            // Get views
            mPositionView = (TextView) v.findViewById(R.id.item_event_position_tv);
            mCoverImage = (ImageView) v.findViewById(R.id.item_event_cover_rl);
            mFavoriteCb = (CheckBox) v.findViewById(R.id.item_event_favorite_cb);
            mTitleView = (TextView) v.findViewById(R.id.item_event_title_tv);
            mTimeView = (TextView) v.findViewById(R.id.item_event_time_tv);
            mNewIcon = (ImageView) v.findViewById(R.id.item_event_new_ic);
            mCategoriesLayout = (CategoriesLayout) v.findViewById(R.id.item_event_categories_layout);
            mFavoriteCountTv = (TextView) v.findViewById(R.id.item_event_favorite_count_tv);
        }
    }
}