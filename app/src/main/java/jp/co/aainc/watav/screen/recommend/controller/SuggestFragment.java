package jp.co.aainc.watav.screen.recommend.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.Requests;
import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.pref.PrefManager;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.common.util.NetworkUtil;
import jp.co.aainc.watav.screen.login.controller.LoginFragment;
import jp.co.aainc.watav.screen.recommend.model.RecommendData;

/**
 * Recommend tab fragment
 * Created by TuanDT on 11/18/2014.
 */
public class SuggestFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = LoginFragment.class.getSimpleName();

    // Sub fragments layout
    private FrameLayout mTopLayout, mStaffLayout, mLatestLayout;

    // Fragment manager
    private FragmentManager mFragmentManager;

    // Sub fragments
    private TopRecommendEventFragment mTopEventFragment;
    private SuggestEventsFragment mStaffEventsFragment, mLatestEventsFragment;

    // Refresh widget
    private SwipeRefreshLayout mSwipeRefreshWidget;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_suggest, container, false);

        mTopLayout = (FrameLayout) view.findViewById(R.id.recommend_top_event_fl);
        mStaffLayout = (FrameLayout) view.findViewById(R.id.recommend_staff_events_fl);
        mLatestLayout = (FrameLayout) view.findViewById(R.id.recommend_latest_events_fl);

        // Refresh widget
        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widget);
        mSwipeRefreshWidget.setColorSchemeResources(R.color.refresh_color1, R.color.refresh_color2,
                R.color.refresh_color3, R.color.refresh_color4);
        mSwipeRefreshWidget.setOnRefreshListener(this);

        DialogUtil.showProgressDialog(getActivity());
        requestRecommendData();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Init children fragments
        mFragmentManager = getChildFragmentManager();
        initFragments();

        if (activity instanceof OnHorizontalScrollListener) {
            OnHorizontalScrollListener listener = (OnHorizontalScrollListener) activity;
            mLatestEventsFragment.setOnRecyclerViewScrollListener(listener);
            mStaffEventsFragment.setOnRecyclerViewScrollListener(listener);
        }
    }

    /**
     *  Get recommend data from server
     */
    private void requestRecommendData() {
        if (NetworkUtil.checkNetwork(getActivity())) {
            mSwipeRefreshWidget.setRefreshing(true);

            // Parameters
            Map<String, String> params = new HashMap<String, String>();
            params.put(Requests.Params.TOKEN, PrefManager.getAccessToken(getActivity()));

            new RecommendRequest(RecommendData.class, params, mRequestListener).send();
        }
    }

    // Request response listener
    private RequestListener mRequestListener = new RequestListener<RecommendData>() {
        @Override
        public void onSuccess(RecommendData data) {
            // Load top event data
            loadTopView(data.getTopEvent());

            // Load staff view
            loadStaffView(data.getRecommendEvents());

            // Load latest view
            loadLatestView(data.getLatestEvents());

            // Hidden refresh widget
            mSwipeRefreshWidget.setRefreshing(false);
        }
    };

    private void initFragments() {
        FragmentTransaction transaction = mFragmentManager.beginTransaction();
        mTopEventFragment = TopRecommendEventFragment.newInstance();
        mStaffEventsFragment = SuggestEventsFragment.newInstance(getString(R.string.title_recommend_staff), false);
        mLatestEventsFragment = SuggestEventsFragment.newInstance(getString(R.string.title_latest), true);

        transaction.add(R.id.recommend_top_event_fl, mTopEventFragment);
        transaction.add(R.id.recommend_staff_events_fl, mStaffEventsFragment);
        transaction.add(R.id.recommend_latest_events_fl, mLatestEventsFragment);
        transaction.commit();
    }

    /**
     * Load top view
     * @param event Top event
     */
    private void loadTopView(Event event) {
        if (event != null) {
            mTopLayout.setVisibility(View.VISIBLE);
            mTopEventFragment.setEvent(event);
        } else {
            mTopLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Load recommend events view
     * @param events List of events
     */
    private void loadStaffView(List<Event> events) {
        // TODO Remove later
//        List<Event> newEvents = new ArrayList<>(events);
//        events.addAll(newEvents);
        if (events == null || events.size() == 0) {
            mStaffLayout.setVisibility(View.GONE);
        } else {
            mStaffLayout.setVisibility(View.VISIBLE);
            mStaffEventsFragment.setEvents(events);
        }
    }

    /**
     * Load latest events view
     * @param events List of events
     */
    private void loadLatestView(List<Event> events) {
        if (events == null || events.size() == 0) {
            mLatestLayout.setVisibility(View.GONE);
        } else {
            mLatestLayout.setVisibility(View.VISIBLE);
            mLatestEventsFragment.setEvents(events);
        }
    }

    @Override
    public void onRefresh() {
        requestRecommendData();
    }
}
