package jp.co.aainc.watav.common.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.model.Category;

/**
 * Custom view to show multi category view .
 * Automatic add categories in new line when not enough space to add
 * Created by TuanDT on 11/18/2014.
 */
public class CategoriesLayout extends LinearLayout {
    private static final String TAG = CategoriesLayout.class.getSimpleName();

    // Common padding
    private int mMarginViews = 0;

    // Categories
    private List<Category> mCategories = new ArrayList<Category>();

    // Width of this view
    private int mRootWidth = 0;

    // Padding
    private int mPaddingLeft, mPaddingRight;

    // Single line or multi line
    private boolean mIsSingleLine;

    // Layout Id
    private int mLayoutId;

    public CategoriesLayout(Context context) {
        super(context);
        init(context, null);
    }

    public CategoriesLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Initialize
     */
    private void init(Context context, AttributeSet attrs) {
        this.setOrientation(VERTICAL);
        mPaddingLeft = mPaddingRight = 0;

        if (attrs != null) {
            // Get padding from xml file
            int[] attributes = new int[]{android.R.attr.padding, android.R.attr.paddingLeft, android.R.attr.paddingRight};
            TypedArray arr = context.obtainStyledAttributes(attrs, attributes);

            int padding = arr.getDimensionPixelOffset(0, 0);
            // Get padding attribute if set
            if (padding > 0) {
                mPaddingLeft = mPaddingRight = padding;
            } else {
                mPaddingLeft = arr.getDimensionPixelOffset(1, 0);
                mPaddingRight = arr.getDimensionPixelOffset(2, 0);
            }

            // Recycle after used
            arr.recycle();
        }

        // Get single line attribute
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CategoriesLayout);
        mIsSingleLine = a.getBoolean(R.styleable.CategoriesLayout_singleLine, false);
        mMarginViews = a.getDimensionPixelSize(R.styleable.CategoriesLayout_marginViews, 0);
        mLayoutId = a.getResourceId(R.styleable.CategoriesLayout_layoutId, R.layout.layout_category_item);
        a.recycle();
    }

//    @Override
//    protected void onSizeChanged(int w, int h, int oldW, int oldH) {
//        super.onSizeChanged(w, h, oldW, oldH);
//        mRootWidth = w;
//    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(parentWidth, parentHeight);
        mRootWidth = parentWidth;
        populateViews(parentWidth);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /**
     * Set categories to be added
     *
     * @param categories Categories added
     */
    public void setCategories(List<Category> categories) {
        mCategories = new ArrayList<>(categories);
        populateViews(mRootWidth);
    }

    /**
     * Add Categories items base on Categories list
     *
     * @param rootWidth Width of root view
     */
    private void populateViews(final int rootWidth) {
        if (rootWidth <= 0) {
            return;
        }

        final int maxWidth = rootWidth - mPaddingLeft - mPaddingRight;

        // Clear view
        this.removeAllViews();

        // Child view layout params
        LinearLayout.LayoutParams params;

        // Create line layout
        LinearLayout lineLayout = createLineLayout();

        int lineWidth = 0;

//        for (Category cat : mCategories) {
        for (Category cat : mCategories) {
            // View to be added
//            TextView view = new TextView(getContext());
//            view.setText(cat);
//            view.setBackgroundColor(Color.BLACK);
            TextView view = createItem(cat.getName());

            // Create holder layout of child view
            LinearLayout childLayout = new LinearLayout(getContext());
            childLayout.setOrientation(LinearLayout.HORIZONTAL);
            childLayout.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
            childLayout.setLayoutParams(new ListView.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

            // Set layout params for view
            view.measure(0, 0);
            params = new LinearLayout.LayoutParams(view.getMeasuredWidth(), LayoutParams.WRAP_CONTENT);

            // Add view to holder & measure width
            childLayout.addView(view, params);
            childLayout.measure(0, 0);

            // Increase current line width
            lineWidth += (view.getMeasuredWidth() + mMarginViews);

            // If greater than max width, add new line layout
            if (lineWidth >= maxWidth) {
                // Only single line is visible
                if (mIsSingleLine) {
                    break;
                }

                // Add line to root
                this.addView(lineLayout);

                lineLayout = createLineLayout();
                addToLine(lineLayout, childLayout);

                // Reset line width
                lineWidth = childLayout.getMeasuredWidth();
            } else {
                // If has enough space, add view
                addToLine(lineLayout, childLayout);
            }
        }

        this.addView(lineLayout);
    }

    /**
     * Add category view to line layout
     *
     * @param lineLayout  Holder
     * @param childLayout Category view
     */
    private void addToLine(LinearLayout lineLayout, LinearLayout childLayout) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(childLayout.getMeasuredWidth(), childLayout.getMeasuredHeight());

        // First item do not need padding
        int marginLeft = lineLayout.getChildCount() == 0 ? 0 : mMarginViews;
        params.setMargins(marginLeft, 0, 0, 0);
        lineLayout.addView(childLayout, params);
    }

    /**
     * Layout hold Category view in a line
     *
     * @return Layout
     */
    private LinearLayout createLineLayout() {
        LinearLayout layout = new LinearLayout(getContext());

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

        // First line do not need margin
        int marginTop = this.getChildCount() == 0 ? 0 : mMarginViews;
        params.setMargins(0, marginTop, 0, 0);
        layout.setLayoutParams(params);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setGravity(Gravity.LEFT);
        return layout;
    }

    /**
     * Create a category view
     *
     * @return View
     */
    private TextView createItem(String title) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        TextView view = (TextView) inflater.inflate(mLayoutId, null);
        view.setText(title);
        return view;
    }
}
