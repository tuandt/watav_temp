package jp.co.aainc.watav.screen.main.controller;

import android.app.ActionBar;
import android.content.Context;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.BaseActivity;

/**
 * Helpers for action bar
 * Created by TuanDT on 12/11/2014.
 */
public class ActionBarHelper {
    private BaseActivity mActivity;
    private final ActionBar mActionBar;
    private ImageView mBackImageView;
    private View mCustomView;
    private TextView mTitleView;
    private SlidingPaneLayout mSlidingLayout;

    public ActionBarHelper(BaseActivity activity) {
        mActivity = activity;
        mActionBar = mActivity.getActionBar();
        mSlidingLayout = mActivity.getPaneLayout();
    }

    public void init() {
        refresh();

        // Custom action bar
        LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCustomView = inflater.inflate(R.layout.action_bar, null);
        mActionBar.setCustomView(mCustomView);

        initViews();
    }

    public void refresh() {
        mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        mActionBar.setDisplayShowCustomEnabled(true);
        mActionBar.setDisplayUseLogoEnabled(false);
        mActionBar.setDisplayHomeAsUpEnabled(false);
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        mActionBar.setHomeButtonEnabled(false);
    }

    public void onPanelClosed() {
        mBackImageView.setVisibility(View.VISIBLE);
    }

    public void onPanelOpened() {
        mBackImageView.setVisibility(View.INVISIBLE);
    }

    public void onFirstLayout() {
        if (mSlidingLayout.isSlideable() && !mSlidingLayout.isOpen()) {
            onPanelClosed();
        } else {
            onPanelOpened();
        }
    }

//    public void hide() {
//        mActionBar.hide();
//    }

//    public void show() {
//        mActionBar.show();
//    }

    public void setTitle(String title) {
        mTitleView.setText(title);
    }
    public void setTitle(int stringResId) {
        mTitleView.setText(mActivity.getString(stringResId));
    }

    public void setColor(int color) {
        mCustomView.setBackgroundColor(color);
    }

    public void setBackground(int backgroundId) {
        mCustomView.setBackgroundResource(backgroundId);
    }

    private void initViews() {
//        //Hides the View (and so the icon)
//        View homeIcon = mActionBar.getCustomView().findViewById(android.R.id.home);
//        ((View)homeIcon.getParent()).setVisibility(View.GONE);
        mActionBar.setIcon(android.R.color.transparent);

        // Time on Calendar
        Date date = new Date();
        String day = new SimpleDateFormat("dd", Locale.JAPAN).format(date);
        String weekDay = new SimpleDateFormat("E", Locale.JAPAN).format(date);

        ((TextView) mCustomView.findViewById(R.id.action_bar_day_tv))
                .setText(day);
        ((TextView) mCustomView.findViewById(R.id.action_bar_week_day_tv))
                .setText(weekDay);

        // Back button
        mBackImageView = (ImageView) mCustomView.findViewById(android.R.id.home);
        mBackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mSlidingLayout.isOpen()) {
                    mSlidingLayout.openPane();
                }
            }
        });

        // Title
        mTitleView = (TextView) mCustomView.findViewById(R.id.action_bar_title_tv);
        mTitleView.setText(R.string.title_recommend);
    }
}
