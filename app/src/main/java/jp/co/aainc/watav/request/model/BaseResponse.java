package jp.co.aainc.watav.request.model;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

/**
 * Common Response data from server
 * Created by TuanDT on 11/18/2014.
 */
public class BaseResponse {
    @SerializedName("success")
    private boolean mSuccess;

    @SerializedName("errors")
    private ErrorResponse mError;

    @SerializedName("data")
    private JsonElement mData;

    /**
     * Return response data
     *
     * @return Data
     */
    public JsonElement getData() {
        return mData;
    }

    /**
     * Request success
     *
     * @return success
     */
    public boolean isSuccess() {
        return mSuccess && mError == null;
    }

    public ErrorResponse getError() {
        return mError;
    }
}
