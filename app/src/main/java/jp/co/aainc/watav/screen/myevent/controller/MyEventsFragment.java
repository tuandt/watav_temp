package jp.co.aainc.watav.screen.myevent.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.ListEventsFragment;
import jp.co.aainc.watav.common.controller.ListPagersAdapter;
import jp.co.aainc.watav.common.controller.PagersFragment;
import jp.co.aainc.watav.log.Logger;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * My Event fragment
 * Created by TuanDT on 12/3/2014.
 */
public class MyEventsFragment extends PagersFragment implements OnUpdateTabWidget {
    private OnHistoryEventsListener mOnHistoryEventsClicked;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        // Visible history event button
        LinearLayout historyLayout = (LinearLayout) view.findViewById(R.id.my_page_history_ll);
        historyLayout.setVisibility(View.VISIBLE);
        historyLayout.setOnClickListener(new OnHistoryClickListener());
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mOnHistoryEventsClicked = getOnHistoryEventsListener();
    }

    /**
     * Find parents handle button History click listener
     *
     * @return OnShowAllClicked
     */
    private OnHistoryEventsListener getOnHistoryEventsListener() {
        Fragment parent = getParentFragment();

        while (parent != null) {
            Logger.e("MyEventsFragment onAttach " + parent);

            if (parent instanceof OnHistoryEventsListener) {
                return (OnHistoryEventsListener) parent;
            } else {
                parent = parent.getParentFragment();
            }
        }

        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tabs_fixed;
    }

    @Override
    protected ListPagersAdapter createAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager viewPager) {
        return new MyEventPagersAdapter(activity, manager, tabHost, viewPager);
    }

    @Override
    protected void initializeData(Activity activity) {
        Bundle args = new Bundle();
        args.putBoolean(ListEventsFragment.KEY_INITIAL_ON_CREATE, false);

        addTab(getString(R.string.title_my_event_favorite), MyFavoritedEventsFragment.class, args, MyFavoritedEventsFragment.class.getName());
        addTab(getString(R.string.title_my_event_join), MyJoinedEventsFragment.class, args, MyJoinedEventsFragment.class.getName());
        addTab(getString(R.string.title_my_event_invited), MyInvitedEventsFragment.class, args, MyInvitedEventsFragment.class.getName());
        notifyDataSetChanged(true);
    }

    @Override
    public void updateTabWidget(int eventsCount) {
        TabWidget widget = mTabHost.getTabWidget();
        View view = widget.getChildTabViewAt(mTabHost.getCurrentTab());

        // Change badge text
        TextView badgeView = (TextView) view.findViewById(R.id.tab_badge_tv);
        badgeView.setText(String.valueOf(eventsCount));
    }

    private class OnHistoryClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            if (mOnHistoryEventsClicked != null)
                mOnHistoryEventsClicked.onHistoryOpen();
        }
    }
}
