package jp.co.aainc.watav.screen.recommend.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.BaseFragment;
import jp.co.aainc.watav.common.controller.OnFavoriteClickListener;
import jp.co.aainc.watav.common.model.Category;
import jp.co.aainc.watav.common.model.Event;
import jp.co.aainc.watav.common.util.ImageUtil;
import jp.co.aainc.watav.common.view.CategoriesLayout;

/**
 * Top Recommend Event
 * Created by TuanDT on 11/24/2014.
 */
public class TopRecommendEventFragment extends BaseFragment {
    private CategoriesLayout mCategoriesLayout;
    private TextView mTitleView;
    private ImageView mCoverImage;
    private TextView mTimeView;
    private CheckBox mFavoriteCheckBox;
    private Event mTopEvent;

    /**
     * Create a new instance of Fragment.
     */
    public static TopRecommendEventFragment newInstance() {
        return new TopRecommendEventFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recommend_top, container, false);

        // Components
        mCategoriesLayout = (CategoriesLayout) view.findViewById(R.id.categories_layout);
        mCoverImage = (ImageView) view.findViewById(R.id.recommend_top_cover_image);
        mTitleView = (TextView) view.findViewById(R.id.recommend_top_title_tv);
        mTimeView = (TextView) view.findViewById(R.id.recommend_top_time_tv);
        mFavoriteCheckBox = (CheckBox) view.findViewById(R.id.recommend_top_favorite_cb);
        return view;
    }

    /**
     * Set event data to be loaded
     *
     * @param event {@link jp.co.aainc.watav.common.model.Event}
     */
    public void setEvent(Event event) {
        mTopEvent = event;
        setCover(mTopEvent.getImageUrl());
        setTitle(mTopEvent.getTitle());
        setTimeDuration(event);
        setCategories(mTopEvent.getCategories());
        setFavoriteStatus(mTopEvent);
    }

    /**
     * Validate url
     *
     * @param url url
     * @return Validated url
     */
    protected String getValidUrl(String url) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }

        return url;
    }

    /**
     * Load cover image
     *
     * @param imageUrl Cover link
     */
    private void setCover(String imageUrl) {
        ImageUtil.loadImageByGlide(getParentFragment().getActivity(), mCoverImage, imageUrl);
    }

    /**
     * Event title
     *
     * @param title Title of event
     */
    private void setTitle(String title) {
        mTitleView.setText(title);
    }

    /**
     * Event duration
     *
     * @param event Event
     */
    private void setTimeDuration(Event event) {
        event.setTimeView(mTimeView, Event.TimeFormat.TIME_FORMAT_START_1_LINE);
    }

    /**
     * Favorite status
     * @param event {@link jp.co.aainc.watav.common.model.Event}
     */
    private void setFavoriteStatus(Event event) {
        mFavoriteCheckBox.setChecked(event.isFavorite());
        mFavoriteCheckBox.setOnClickListener(new OnFavoriteClickListener(getParentFragment().getActivity(), event, null));
    }
    /**
     * Set event Categories
     * @param categories List of {@link jp.co.aainc.watav.common.model.Category}
     */
    public void setCategories(List<Category> categories) {
        mCategoriesLayout.setCategories(categories);
    }
}
