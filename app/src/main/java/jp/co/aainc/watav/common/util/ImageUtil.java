package jp.co.aainc.watav.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.log.Logger;

/**
 * Image utilities
 * Created by TuanDT on 11/20/2014.
 */
public class ImageUtil {
    private static final String HTTP_PREFIX = "http://";
    private static final String HTTPS_PREFIX = "https://";

    /**
     * Crop image fit to requested width & crop height to fit with requested height
     *
     * @param bitmap
     * @param requestW
     * @return
     */
    public static Bitmap cropImage(final Bitmap bitmap, int requestW, int requestH) {
        // Source bitmap size
        int bitmapW = bitmap.getWidth();
        int bitmapH = bitmap.getHeight();

        // Use max scale to scale image
        float scaleX = requestW * 1.0f / bitmapW;
        float scaleY = requestH * 1.0f / bitmapH;
        float scale = scaleX > scaleY ? scaleX : scaleY;

        // Scale bitmap
        int w = (int) (bitmapW * scale);
        int h = (int) (bitmapH * scale);
        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, w, h, true);

        // Crop
        int cropSize;
        Bitmap output;
        if (w > requestW) {
            // Bitmap width greater than request width
            cropSize = (w - requestW) / 2;
            output = Bitmap.createBitmap(scaledBitmap, cropSize, 0, requestW, requestH);
        } else {
            // Bitmap height greater than request height
            cropSize = (h - requestH) / 2;
            output = Bitmap.createBitmap(scaledBitmap, 0, cropSize, requestW, requestH);
        }

        return output;
    }

    /**
     * Load image background by Glide
     *
     * @param context Context
     * @param image   Image to be loaded
     * @param url     Image url
     */
    public static void loadImageByGlide(final Context context, final ImageView image, String url) {
        Glide.with(context)
                .load(getValidUrl(url))
                .centerCrop()
                .crossFade()
                .into(image);
    }


    public static void loadImageWithGlideAndBlur(final Context context, final String url, final ImageView image) {
        String validUrl = getValidUrl(url);
        image.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        int width = image.getMeasuredWidth();
        int height = image.getMeasuredHeight();
        Logger.e("Load glide blur " + validUrl);
        Logger.i("w-h " + width + "-" + height);
        Glide.with(context)
                .load(validUrl)
                .asBitmap()
                .placeholder(R.drawable.bg_cover_default)
                .error(R.drawable.bg_cover_default)
                .transform(new BlurTransformation(context, width, height, validUrl))
                .into(image);
    }

    private static class BlurTransformation extends BitmapTransformation {
        private String mUrl;
        private static final int BLUR_RADIUS = 38;
        private int mTargetWidth, mTargetHeight;

        public BlurTransformation(Context context, int targetWidth, int targetHeight, String url) {
            super(context);
            mUrl = url;
            mTargetWidth = targetWidth;
            mTargetHeight = targetHeight;
        }

        @Override
        protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
            Logger.e("toTransform " + toTransform.getWidth() + "-" + toTransform.getHeight());
            Logger.e("Target " + mTargetWidth + "-" + mTargetHeight);
            Bitmap cropBitmap = cropImage(toTransform, mTargetWidth, mTargetHeight);

            Bitmap blurredBitmap = fastBlur(cropBitmap, BLUR_RADIUS);
            Logger.e("blurredBitmap " + blurredBitmap.getWidth() + "-" + blurredBitmap.getHeight());
            return blurredBitmap;
        }

        @Override
        public String getId() {
            return "blur " + mUrl;
        }
    }

    /**
     * Validate url
     *
     * @param url url
     * @return Validated url
     */
    private static String getValidUrl(String url) {
        if (url == null) {
            return null;
        }

        if (!url.startsWith(HTTP_PREFIX) && !url.startsWith(HTTPS_PREFIX)) {
            url = HTTP_PREFIX + url;
        }

        return url;
    }

    // TODO check later
    private static Bitmap fastBlur(Bitmap sentBitmap, int radius) {
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Logger.e("pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Logger.e("pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }
}
