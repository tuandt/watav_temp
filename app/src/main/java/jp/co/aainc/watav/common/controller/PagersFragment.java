package jp.co.aainc.watav.common.controller;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.util.DialogUtil;
import jp.co.aainc.watav.common.view.CustomViewPager;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * Base Tabs Fragments
 * Created by TuanDT on 12/4/2014.
 */
public abstract class PagersFragment extends BaseFragment {
    private boolean mIsInitialized = false;

    // Tabs & Pages
    protected TabHost mTabHost;
    protected CustomViewPager mViewPager;
    protected PagersAdapter mPagersAdapter;
    protected MainActivity mActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();

        // Init view
        View view = inflater.inflate(getLayoutId(), container, false);
        mTabHost = (TabHost) view.findViewById(android.R.id.tabhost);
        mTabHost.setup();

        mViewPager = (CustomViewPager) view.findViewById(R.id.pager);

        mPagersAdapter = createAdapter(mActivity, getChildFragmentManager(), mTabHost, mViewPager);
        if (mPagersAdapter == null) {
            mPagersAdapter = createDefaultAdapter();
        }

        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab"));
        }

        // ViewPager Scroll Listener
        mViewPager.setOnHorizontalScrollListener(mActivity);

        return view;
    }

    protected abstract int getLayoutId();

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tab", mViewPager.getCurrentItem());
    }

    /**
     * Custom Adapter
     * @return
     */
    protected abstract  ListPagersAdapter createAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager viewPager);

    /**
     * Default adapter
     * @return
     */
    protected ListPagersAdapter createDefaultAdapter() {
        return new ListPagersAdapter(mActivity, getChildFragmentManager(), mTabHost, mViewPager);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        onCreateData(mActivity);
    }

    public void onCreateData(Activity activity) {
        if (!mIsInitialized) {
            DialogUtil.showProgressDialog(activity);
            initializeData(activity);
            mIsInitialized = true;
        }
    }

    protected abstract void initializeData(Activity activity);

    protected void addTab(String title, Class<?> clazz, Bundle args, String tag) {
        mPagersAdapter.addTab(title, clazz, args, tag);
    }

    protected void notifyDataSetChanged(boolean firstCreation) {
        mPagersAdapter.notifyDataSetChanged();
        if (firstCreation) {
            mPagersAdapter.onPageSelected(0);
        }
    }
}
