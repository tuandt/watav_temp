package jp.co.aainc.watav.screen.recommend.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import jp.co.aainc.watav.common.model.Event;

/**
 * Recommends data
 * Created by TuanDT on 11/20/2014.
 */
public class RecommendData {
    @SerializedName("top")
    private List<Event> mTop;

    @SerializedName("recomment")
    private List<Event> mRecommends;

    @SerializedName("new")
    private List<Event> mLatestEvents;

    public Event getTopEvent() {
        if (mTop != null && mTop.size() > 0) {
            return mTop.get(0);
        }

        return null;
    }

    public List<Event> getRecommendEvents() {
        return mRecommends;
    }

    public List<Event> getLatestEvents() {
        return mLatestEvents;
    }
}
