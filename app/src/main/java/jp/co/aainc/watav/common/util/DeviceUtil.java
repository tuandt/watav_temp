package jp.co.aainc.watav.common.util;

import android.content.Context;
import android.graphics.Point;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.WindowManager;

/**
 * Device utilities
 * Created by TuanDT on 11/13/2014.
 */
public class DeviceUtil {
    /**
     * Get device id
     * @param context To get TELEPHONY_SERVICE
     * @return id of device
     */
    public static String getDeviceId(Context context) {
        if (context == null) {
            return "";
        }

        TelephonyManager telephonyManager = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    /**
     * Get screen size as {@link android.graphics.Point} object. x is width, y is height
     * @param context to get service
     * @return Screen size
     */
    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size;
    }
}
