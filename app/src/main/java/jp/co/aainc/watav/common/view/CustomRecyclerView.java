package jp.co.aainc.watav.common.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import jp.co.aainc.watav.screen.recommend.controller.OnHorizontalScrollListener;

/**
 * Custom {@link android.support.v7.widget.RecyclerView}
 * Created by TuanDT on 12/8/2014.
 */
public class CustomRecyclerView extends RecyclerView implements View.OnTouchListener{
    private OnHorizontalScrollListener mOnHorizontalScrollListener;

    public CustomRecyclerView(Context context) {
        super(context);
    }

    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setOnHorizontalScrollListener(OnHorizontalScrollListener listener) {
        mOnHorizontalScrollListener = listener;
        this.setOnScrollListener(new MyOnScrollListener());
        this.setOnTouchListener(this);
    }

    private boolean checkOnTouch() {
        if (mOnHorizontalScrollListener == null) {
            return false;
        }

        LinearLayoutManager layoutManager = (LinearLayoutManager) this.getLayoutManager();
        int firstFullVisible = layoutManager.findFirstCompletelyVisibleItemPosition();
        int lastFullVisible = layoutManager.findLastCompletelyVisibleItemPosition();

        if (firstFullVisible == 0) {
            mOnHorizontalScrollListener.onAtStartPosition();
        } else if (lastFullVisible == layoutManager.getItemCount() - 1) {
            mOnHorizontalScrollListener.onAtEndPosition();
        } else {
            mOnHorizontalScrollListener.onAtMiddlePosition();
        }
        return false;
    }
    @Override
    public boolean onTouch(View view, MotionEvent event) {
        return checkOnTouch();
    }

    private class MyOnScrollListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (mOnHorizontalScrollListener != null) {
                mOnHorizontalScrollListener.onScrolled();
            }
        }
    }
}
