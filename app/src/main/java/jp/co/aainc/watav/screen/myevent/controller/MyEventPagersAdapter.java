package jp.co.aainc.watav.screen.myevent.controller;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import jp.co.aainc.watav.R;
import jp.co.aainc.watav.common.controller.ListPagersAdapter;
import jp.co.aainc.watav.screen.main.controller.MainActivity;

/**
 * My Events ViewPager Adapter
 * Created by TuanDT on 12/5/2014.
 */
public class MyEventPagersAdapter extends ListPagersAdapter {
    public MyEventPagersAdapter(MainActivity activity, FragmentManager manager, TabHost tabHost, ViewPager pager) {
        super(activity, manager, tabHost, pager);
    }

    @Override
    public void onTabChanged(String tabId) {
        super.onTabChanged(tabId);

        TabWidget widget = mTabHost.getTabWidget();
        Context context = mTabHost.getContext();

        // Change Tab view components
        for (int i = 0; i < widget.getTabCount(); i++) {
            View view = widget.getChildTabViewAt(i);
            TextView titleView = (TextView) view.findViewById(R.id.tab_title_tv);
            TextView badgeView = (TextView) view.findViewById(R.id.tab_badge_tv);

            int titleColor, badgeColor, badgeResourceId;

            Resources res = context.getResources();
            if (i == mTabHost.getCurrentTab()) {
                titleColor = res.getColor(R.color.tab_title_text_selected);
                badgeColor = res.getColor(R.color.tab_badge_text_selected);
                badgeResourceId = R.drawable.bg_tab_badge_selected;
            } else {
                titleColor = res.getColor(R.color.tab_title_text_unselected);
                badgeColor = res.getColor(R.color.tab_badge_text_unselected);
                badgeResourceId = R.drawable.bg_tab_badge_unselected;
            }

            titleView.setTextColor(titleColor);
            badgeView.setTextColor(badgeColor);
            badgeView.setBackgroundResource(badgeResourceId);
        }
    }

    @Override
    public void addTab(String title, Class<?> clazz, Bundle args, String tag) {
        TabInfo info = new TabInfo(tag, clazz, args);
        mTabs.add(info);
        if (mTabHost.getChildCount() > 0)
            notifyDataSetChanged();

        // Keep number of page states
        mViewPager.setOffscreenPageLimit(mTabs.size());
        setNewTab(mTabHost, tag, title);
    }

    private void setNewTab(TabHost tabHost, String tag, String title) {
        TabHost.TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getTabIndicator(tabHost.getContext(), title));
        tabSpec.setContent(new TabFactory(mActivity));
        tabHost.addTab(tabSpec);
    }

    private View getTabIndicator(Context context, String title) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_tab_myevent, null);
        TextView titleView = (TextView) view.findViewById(R.id.tab_title_tv);
        titleView.setText(title);
        return view;
    }
}
