package jp.co.aainc.watav.request.manager;

import android.app.Activity;

import java.util.Map;

import jp.co.aainc.watav.common.util.DialogUtil;

/**
 * Must wait for request complete. Show progress dialog when requesting
 * Created by TuanDT on 11/20/2014.
 */
public abstract class ForegroundRequest<T> extends BaseRequest<T>{
    private Activity mActivity;

    public ForegroundRequest(Activity activity, Class clazz, Map params, BaseListener listener) {
        super(clazz, params, listener);
        mActivity = activity;
    }

    @Override
    public void send() {
        DialogUtil.showProgressDialog(mActivity);
        super.send();
    }
}
